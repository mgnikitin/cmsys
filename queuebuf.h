/** \file queuebuffer.h
  * \brief Статический буфер типа очередь (FIFO)
  * \author Никитин М. Г., отд. 023
  * \version 1.1
  */

#ifndef QUEUEBUFFER_H
#define QUEUEBUFFER_H

#include "common.h"
/**
  * \class CQueueBuffer
  * \brief Статический буфер типа очередь (FIFO)
  */
class CQueueBuffer
{
public:
    CQueueBuffer(const unsigned short &size);
    ~CQueueBuffer();

    /// \brief Поместить байт в буфер
    bool set(const uint8_t &b);

    /** \brief Поместить массив в буфер
      * \return Количество помещенных байт в буфер
      */
    unsigned short set(uint8_t *first, unsigned short count);

    /// \brief Прочесть байт из буфера
    bool get(uint8_t &b);

    /** \brief Поиск массива в буфере
      * Производится чтение буфера до позиции элемента,
      * начиная с которого символы совпадают с \ref arr.
      * Если массив не найден буфер становится пустым.
      * \return true - массив найден, false - иначе
      */
    bool find(uint8_t *arr, unsigned int size);

    bool silent_find(uint8_t *arr, unsigned int size);

    /// \brief Вернуть количество использованных байт буфера
    unsigned short used_count() const;

    /// \brief Вернуть количество доступных байт буфера
    unsigned short avail_count() const;

    /// \brief Вернуть размер буфера
    unsigned short size() const;

private:

    uint8_t *arr;               ///< \brief Массив буфера
    unsigned short offset_set; ///< \brief Смещение позиции записи
    unsigned short offset_get; ///< \brief Смещение позиции чтения
    unsigned short _count;     ///< \brief Размер занятой части буфера
    const unsigned short sz;   ///< \brief Размер буфера

    /// \brief Передвинуть позицию чтения/записи вправо, при достижении конца массива
    /// позиция чтения/записи смещается на нулевой элемент (циклический сдвиг)
    void move_right(unsigned short &offset);

    /** \brief Вернуть атрибуты буфера
      * \var count - Размер занятой части буфера
      * \var offst_get - Смещение позиции чтения
      */
    void get_attrib(unsigned short &count, unsigned short &offst_get) const;

    /** \brief Установить атрибуты буфера
      * \var count - Размер занятой части буфера
      * \var offst_get - Смещение позиции чтения
      */
    void set_attrib(const unsigned short &count, const unsigned short &offst_get);
};

#endif // QUEUEBUFFER_H
