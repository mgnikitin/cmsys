/* 
 * File:   daemonapp.cpp
 * Author: user
 * 
 * Created on 1 Ноябрь 2014 г., 11:48
 */

#include <QMessageLogContext>
#include "daemonapp.h"
#include "trace.h"
#include <sys/types.h> 
#include <sys/socket.h>
#include <errno.h>
#include "config.h"

int CDaemonApp::fd_sighup[2] = {-1, -1};
int CDaemonApp::fd_sigterm[2] = {-1, -1};

void myMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    char str[1024];
    QByteArray localMsg = msg.toLocal8Bit();
    snprintf(str, 1024, "%s (%s:%u, %s)", localMsg.constData(), context.file, context.line, context.function);
    str[1023] = '\0';
    switch (type) {
    case QtDebugMsg:
        trace(L_DEBUG, str);
    break;
    case QtInfoMsg:
        trace(L_NOTIFY, str);
        break;
    case QtWarningMsg:
        trace(L_WARNING, str);
    break;
    case QtCriticalMsg:
        trace(L_ERROR, str);
    break;
    case QtFatalMsg:
        trace(L_ERROR, str);
        abort();
    }
}

CDaemonApp::CDaemonApp(int & argc, char ** argv) 
: QCoreApplication(argc, argv)
{    
    const char *conf_str = "/etc/cmsysd.conf";
    
    qInstallMessageHandler(myMessageHandler);
    
    trace(L_DEBUG, "CDaemonApp");
    sighup_notifier = NULL;
    sigterm_notifier = NULL;
    
    t_config conf = Config::defaultConfig();
    if(!Config::readConfig(conf_str, &conf)) {
        conf = Config::defaultConfig();
        trace(L_ERROR, QObject::trUtf8("CMSMngr::CMSMngr(): Couldn't read config %1")
                .arg(QObject::trUtf8(conf_str)).toLatin1());
    }
    
    cms_mngr = new CMSMngr(conf);
        
    QObject::connect(cms_mngr, SIGNAL(s_close()), this, SLOT(quit()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(aboutToQuit()), cms_mngr, SLOT(slot_quit()));
    
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, fd_sighup) == -1)
    {
        trace(L_ERROR, QObject::trUtf8("Couldn't create HUP socketpair: %1(%2)")
                .arg(strerror(errno)).arg(errno).toLatin1());
        return;
    }

     if (::socketpair(AF_UNIX, SOCK_STREAM, 0, fd_sigterm) == -1)
     {
        trace(L_ERROR, QObject::trUtf8("Couldn't create TERM socketpair: %1(%2)")
                .arg(strerror(errno)).arg(errno).toLatin1());
        return;
     }
    
    sighup_notifier = new QSocketNotifier(fd_sighup[1], QSocketNotifier::Read, this);
    connect(sighup_notifier, SIGNAL(activated(int)), 
            this, SLOT(slot_sighup(int)));
    sigterm_notifier = new QSocketNotifier(fd_sigterm[1], QSocketNotifier::Read, this);
    connect(sigterm_notifier, SIGNAL(activated(int)), 
            this, SLOT(slot_sigterm(int)));
    
    cms_mngr->start();
}

CDaemonApp::~CDaemonApp() 
{
    trace(L_DEBUG, "~CDaemonApp");
    
    if(sighup_notifier)
        delete sighup_notifier;
    
    if(sigterm_notifier)
        delete sigterm_notifier;
    
    delete cms_mngr;
}

void CDaemonApp::sighup_handler(int)
{
    trace(L_TRACE, "CDaemonApp::sighup_handler");
    char a = 1;
    ::write(fd_sighup[0], &a, sizeof(a));
}
void CDaemonApp::sigterm_handler(int)
{
    trace(L_TRACE, "CDaemonApp::sigterm_handler");
    char a = 1;
    ::write(fd_sigterm[0], &a, sizeof(a));
}

void CDaemonApp::slot_sighup(int)
{
    trace(L_TRACE, "CDaemonApp::slot_sighup");
    
    sighup_notifier->setEnabled(false);
    
    char tmp;
    ::read(fd_sighup[1], &tmp, sizeof(tmp));
     
    emit quit();
    
    sighup_notifier->setEnabled(true);
}

void CDaemonApp::slot_sigterm(int)
{
    trace(L_TRACE, "CDaemonApp::slot_sigterm");
    
    sigterm_notifier->setEnabled(false);
    
    char tmp;
    ::read(fd_sigterm[1], &tmp, sizeof(tmp));
     
    emit quit();
    
    sigterm_notifier->setEnabled(true);
}
