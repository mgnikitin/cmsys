#include "prot.h"
#include "trace.h"
#include <string>
#include <cstdio>

const char *header = "$PCMS";

// ----------------------------------------------------------------------------
CProt::CProt()
    : buf_all(BUFFER_IN_SIZE)
{
    sent_sz = 5;
    sent[0].header = QString("PRTRMD");
    sent[0].func = &CProt::parse_RTRMD;
    sent[1].header = QString("PCNSMD");
    sent[1].func = &CProt::parse_CNSMD;
    sent[2].header = QString("PCMSMD");
    sent[2].func = &CProt::parse_CMSMD;
    sent[3].header = QString("PRTRMD");
    sent[3].func = &CProt::parse_RTRMD;
    sent[4].header = QString("PRTRDT");
    sent[4].func = &CProt::parse_RTRDT;
}

// ----------------------------------------------------------------------------
CProt::~CProt() {

}

// ----------------------------------------------------------------------------
unsigned short CProt::remove_overflow(unsigned short bufferBytesCount, CQueueBuffer &buf)
{
    unsigned short avail_c = buf.avail_count();
    if(avail_c < bufferBytesCount)
    {
        uint8_t b;
        unsigned short expand_c = bufferBytesCount - avail_c;
        for(unsigned short jj = 0; jj < expand_c; jj++)
        {
            buf.get(b);
        }
        return expand_c;
    }
    return 0;
}

// ----------------------------------------------------------------------------
bool CProt::parse_hex(const uint8_t *arr, size_t sz, t_hex &val) {

    unsigned int ui;
    char str[2];
    uint8_t b;

    str[1] = '\0';

    if((sz > MAX_HEX_FIELD_SZ) || (sz == 0)) {
        trace(L_ERROR, QObject::trUtf8("CProt::parse_hex(): invalid size %1")
                .arg(sz).toLatin1());
        return false;
    }

    memset(&val, 0, sizeof(t_hex));

    for(size_t i = 0; i < sz; i++) {

        size_t nm = sz-i-1;
        str[0] = (char)arr[i];
        if(sscanf(str, "%x", &ui) != 1)
            return false;
        b = ui;
        b = (nm % 2) ? b << 4 : b;
        val.arr[nm/2] |= b;
    }
    return true;
}

// ----------------------------------------------------------------------------
bool CProt::parse_content(uint8_t *arr, size_t sz, int &fld_num) {

    size_t idx = 0;
    fld_num = 0;

    //trace(L_TRACE, QObject::trUtf8("arr: %1").arg(QString(QByteArray((char *)arr, sz))).toAscii());

    while(idx < sz) {

        if(((char)arr[idx] == ',') || ((char)arr[idx] == '*'))
            idx++;      // пропускаем разделитель

        size_t fld_len = field_bound(&arr[idx], sz - idx);

        arr_fld[fld_num].len = fld_len;
        arr_fld[fld_num].ptr = (0 == fld_len) ? NULL : &arr[idx];

        idx += fld_len;

//        trace(L_TRACE, QObject::trUtf8("field: %1")
//                .arg(QString::fromLocal8Bit((const char *)arr_fld[fld_num].ptr, arr_fld[fld_num].len)).toLatin1());
        fld_num++;

        if(fld_num >= MAX_FIELD_COUNT) {
            trace(L_ERROR, "CProt::parse_content(): exceed field count");
            return false;
        }
    }

    if(idx != sz) {
        trace(L_ERROR, QObject::trUtf8("CProt::parse_content() idx %1, sz %2")
                .arg(idx).arg(sz).toLatin1());
        return false;
    }

    return true;
}

// ----------------------------------------------------------------------------
size_t CProt::field_bound(uint8_t *arr, size_t sz) {

    size_t idx = 0;
    while(idx < sz) {
        if(((char)arr[idx] == ',') || ((char)arr[idx] == '*'))
            break;
        idx++;
    }
    return idx;
}

// ----------------------------------------------------------------------------
bool CProt::parse_CNSMD(const CProt::t_field *arr_fld, size_t fld_cnt) {

    // $PCNSMD,01,02,0F*12
    uint8_t addr_trg;
    uint8_t addr_src;
    TYPE_CMD cmd;
    t_hex hex;

    if(fld_cnt != 4) {
        trace(L_ERROR, QObject::trUtf8("CProt::parse_CNSMD(): field count mismatch %1")
                .arg(fld_cnt).toLatin1());
        return false;
    }

    // 0
    if(arr_fld[0].len != 6) {
        trace(L_ERROR, QObject::trUtf8("CProt::parse_CNSMD(): header length mismatch %1")
                .arg(arr_fld[0].len).toLatin1());
        return false;
    }
    if(memcmp(arr_fld[0].ptr, "PCNSMD", 6) != 0) {
        trace(L_ERROR, QObject::trUtf8("CProt::parse_CNSMD(): header mismatch %1")
                .arg(QString(QByteArray((char *)arr_fld[0].ptr, arr_fld[0].len))).toLatin1());
        return false;
    }

    // 1
    if(!parse_hex(arr_fld[1].ptr, arr_fld[1].len, hex))
        return false;
    addr_trg = hex.arr[0];

    // 2
    if(!parse_hex(arr_fld[2].ptr, arr_fld[2].len, hex))
        return false;
    addr_src = hex.arr[0];

    // 3
    if(!parse_hex(arr_fld[3].ptr, arr_fld[3].len, hex))
        return false;
    cmd = (TYPE_CMD)hex.arr[0];

    emit s_new_cmd(CommandGenerator::generate(addr_src, addr_trg, cmd));

    return true;
}

// ----------------------------------------------------------------------------
bool CProt::parse_CMSMD(const CProt::t_field *arr_fld, size_t fld_cnt)
{
    /* $PCMSMD,02,01,0F*12
        |   |  |  |  |  |
     *  |   |  |  |  |  |
     *  |   |  |  |  |  +-- checksum
     *  |   |  |  |  +----- command type
     *  |   |  |  +-------- address of sender
     *  |   |  +----------- destenation address
     *  |   +-------------- type of sentence (header part)
     *  +------------------ id of sender (header part)
     */

    uint8_t addr_trg;
    uint8_t addr_src;
    TYPE_CMD cmd;
    t_hex hex;

    if(fld_cnt != 4)
        return false;

    // 0
    if(arr_fld[0].len != 6)
        return false;
    if(memcmp(arr_fld[0].ptr, "PCMSMD", 6) != 0)
        return false;

    // 1
    if(!parse_hex(arr_fld[1].ptr, arr_fld[1].len, hex))
        return false;
    addr_trg = hex.arr[0];

    // 2
    if(!parse_hex(arr_fld[2].ptr, arr_fld[2].len, hex))
        return false;
    addr_src = hex.arr[0];

    // 3
    if(!parse_hex(arr_fld[3].ptr, arr_fld[3].len, hex))
        return false;
    cmd = (TYPE_CMD)hex.arr[0];

    emit s_new_cmd(CommandGenerator::generate(addr_src, addr_trg, cmd));

    return true;
}

// ----------------------------------------------------------------------------
bool CProt::parse_RTRMD(const CProt::t_field *arr_fld, size_t fld_cnt)
{
    /* $PRTRMD,02,01,0F,01*12
        |   |  |  |  |     |
     *  |   |  |  |  |     |
     *  |   |  |  |  |     +-- checksum
     *  |   |  |  |  +-------- command type
     *  |   |  |  +----------- address of sender
     *  |   |  +-------------- destenation address
     *  |   +----------------- type of sentence (header part)
     *  +--------------------- id of sender (header part)
     */

    t_hex hex;
    uint8_t addr_trg;
    uint8_t addr_src;
    TYPE_CMD cmd;
    TYPE_CMD resp_cmd;

    if(fld_cnt != 5)
        return false;

    // 0
    if(arr_fld[0].len != 6)
        return false;
    if(memcmp(arr_fld[0].ptr, "PRTRMD", 6) != 0)
        return false;

    // 1
    if(!parse_hex(arr_fld[1].ptr, arr_fld[1].len, hex))
        return false;
    addr_trg = hex.arr[0];

    // 2
    if(!parse_hex(arr_fld[2].ptr, arr_fld[2].len, hex))
        return false;
    addr_src = hex.arr[0];

    // 3
    if(!parse_hex(arr_fld[3].ptr, arr_fld[3].len, hex))
        return false;
    cmd = (TYPE_CMD)hex.arr[0];

    // 4
    if(!parse_hex(arr_fld[4].ptr, arr_fld[4].len, hex))
        return false;
    resp_cmd = (TYPE_CMD)hex.arr[0];

    emit s_new_cmd(CommandGenerator::generate(addr_src, addr_trg, cmd, resp_cmd));

    return true;
}

// ----------------------------------------------------------------------------
bool CProt::parse_RTRDT(const CProt::t_field *arr_fld, size_t fld_cnt) {

    /* $PRTRDT,02,01,0F,01*12
        |   |  |  |  |     |
     *  |   |  |  |  |     |
     *  |   |  |  |  |     +-- checksum
     *  |   |  |  |  +-------- command type
     *  |   |  |  +----------- address of sender
     *  |   |  +-------------- destenation address
     *  |   +----------------- type of sentence (header part)
     *  +--------------------- id of sender (header part)
     */

    t_hex hex;
    uint8_t addr_trg;
    uint8_t addr_src;
    TYPE_CMD cmd;
    TYPE_CMD resp_cmd;

    if(fld_cnt != 5)
        return false;

    // 0
    if(arr_fld[0].len != 6)
        return false;
    if(memcmp(arr_fld[0].ptr, "PRTRDT", 6) != 0)
        return false;

    // 1
    if(!parse_hex(arr_fld[1].ptr, arr_fld[1].len, hex))
        return false;
    addr_trg = hex.arr[0];

    // 2
    if(!parse_hex(arr_fld[2].ptr, arr_fld[2].len, hex))
        return false;
    addr_src = hex.arr[0];

    // 3
    if(!parse_hex(arr_fld[3].ptr, arr_fld[3].len, hex))
        return false;
    cmd = (TYPE_CMD)hex.arr[0];

    // 4
    if(!parse_hex(arr_fld[4].ptr, arr_fld[4].len, hex))
        return false;
    resp_cmd = (TYPE_CMD)hex.arr[0];

    emit s_new_cmd(CommandGenerator::generate(addr_src, addr_trg, cmd, resp_cmd));

    return true;
}

// ----------------------------------------------------------------------------
int CProt::make_CMSMD(uint8_t *arr, cmd_t cmd)
{
    int len = 0;
    char str[10];
    uint8_t cs;

    memcpy(&arr[len], header, strlen(header));
    len += strlen(header);
    memcpy(&arr[len], "MD", 2);
    len += 2;
    arr[len++] = ',';
    sprintf(str, "%02x", cmd.addr_trg);
    arr[len++] = str[0];
    arr[len++] = str[1];
    arr[len++] = ',';
    sprintf(str, "%02x", cmd.addr_src);
    arr[len++] = str[0];
    arr[len++] = str[1];
    arr[len++] = ',';
    sprintf(str, "%02x", (uint8_t)cmd.cmd);
    arr[len++] = str[0];
    arr[len++] = str[1];
    cs = CalcCS(&arr[1], (size_t)len-1);
    arr[len++] = '*';
    sprintf(str, "%02x", cs);
    arr[len++] = str[0];
    arr[len++] = str[1];
    arr[len++] = '\r';
    arr[len++] = '\n';
    arr[len] = '\0';
    //trace(L_TRACE, "CProt::make_CMSMD(): sent: %s", arr);
    return len;
}

// ----------------------------------------------------------------------------
void CProt::parse_sentence(CQueueBuffer &buf)
{
    uint8_t b = 0;
    int idx = 0;
    uint8_t arr[BUFFER_IN_SIZE];
    t_hex csr;

    if(!buf.get(b))
    {
        trace(L_ERROR, "CProt::parse_sentence(): Couldn't get from buffer");
        return;
    }
    if(b != '$') {
        trace(L_ERROR, "CProt::parse_sentence(): Couldn't find start symbol");
        return;
    }
    // копируем содержимое буфера в массив
    while(true)
    {
        if(!buf.get(b))
        {
            trace(L_ERROR, "CProt::parse_sentence(): Couldn't get from buffer");
            return;
        }

        if(('\n' == b) || ('\r' == b))
            break;

        arr[idx++] = b;

        if(idx >= BUFFER_IN_SIZE)
        {
            trace(L_ERROR, "CProt::parse_sentence(): array overflow");
            return;
        }
    }

    if(idx < 10)
        return;     // слишком малый размер сообщения

    //trace(L_TRACE, QObject::trUtf8("array: %1").arg(QString::fromLocal8Bit((char *)arr, idx)).toLatin1());

    int fld_num;

    if(!parse_content(arr, (size_t)idx, fld_num)) {
        trace(L_DEBUG, "CProt::parse_sentence(): Couldn't parse content");
        return;
    }

    if(fld_num < 2) {
        trace(L_DEBUG, "CProt::parse_sentence(): too few field num");
        return;
    }

    // проверка контрольной суммы
    if(!parse_hex(arr_fld[fld_num-1].ptr, arr_fld[fld_num-1].len, csr)) {
        trace(L_ERROR, "CProt::parse_sentence(): Couldn't read checksum");
        return;
    }

    // расчет контрольной суммы всех символов
    uint8_t cs = CalcCS(&arr[0], idx);

    if(csr.arr[0] != cs)
    {
        trace(L_ERROR, "CProt::parse_sentence(): checksum mismatch, received: %hhu, calced: %hhu",
                csr.arr[0], cs);
        return;
    }

    for(int i = 0; i < sent_sz; i++)
    {

        const t_sent &sent_curr = sent[i];

        if((size_t)sent_curr.header.length() != arr_fld[0].len) {
            trace(L_DEBUG, "CProt::parse_sentence(): header size %lu", (unsigned long)(sent[i].header.length()));
            continue;
        }
        if(sent_curr.header == QString::fromLocal8Bit((const char *)arr_fld[0].ptr, arr_fld[0].len)) {
            //trace(L_TRACE, "CProt::parse_sentence(): header match");
            if((this->*sent_curr.func)(arr_fld, fld_num-1))
                break;
        }
        else {
//            trace(L_TRACE, QObject::trUtf8("CProt::parse_sentence(): header mismatch %1 %2")
//                    .arg(QString(sent_curr.header)).arg(QString(QByteArray((char *)arr_fld[0].ptr, arr_fld[0].len))).toAscii());
        }
    }
}

// ----------------------------------------------------------------------------
uint8_t CProt::CalcCS(const uint8_t *arr, size_t sz)
{
    uint8_t cs = 0;
    for(size_t i = 0; i < sz; i++)
    {
        if((arr[i] == '*') || (arr[i] == '\r'))
            break;
        cs ^= arr[i];
    }
    return cs;
}

// ----------------------------------------------------------------------------
void CProt::loop_find(CQueueBuffer &qbuf) {

    while(true)
    {
        // производим поиск символа начала предложения
        if(!qbuf.find((uint8_t *)"$", 1))
            break;
        //trace(L_TRACE, "Accept header");
        // производим поиск символа конца предложения
        if(!qbuf.silent_find((uint8_t *)"\n", 1))
            break;
        //trace(L_TRACE, "Accept tail");
        // разбор предложения
        parse_sentence(qbuf);
    }
}

// ----------------------------------------------------------------------------
void CProt::slot_parse(QByteArray ba) {

    unsigned short erased = 0;
    unsigned short passed = 0;

    // освобождаем часть буфера, если не хватает места
    erased = remove_overflow((unsigned short)ba.size(), buf_all);
    if(erased > 0)
        trace(L_WARNING, "CProt::slot_parse(): erase bytes: %hu \n", erased);
    // накапливаем данные в буфере
    passed = buf_all.set((uint8_t *)ba.constData(), (unsigned short)ba.size());
    if(passed > 0) {
//        trace(L_TRACE, "CProt::slot_parse(): passed: %hu bytes", passed);
        // произвоидм поиск сообщений
        loop_find(buf_all);
    }
}
