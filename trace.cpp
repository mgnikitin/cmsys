/* 
 * File:   trace.cpp
 * Author: user
 * 
 * Created on 29 Октябрь 2014 г., 18:37
 */

#include "trace.h"
#include <stdarg.h>
#include <syslog.h>

static LOG_LEVEL _lev = L_DEBUG;
static LOG_FACILITY map_fac = LF_ALL;

void setup_trace(const char * ident, LOG_LEVEL lev, LOG_FACILITY facility_map)
{
    _lev = lev;
    map_fac = facility_map;
    openlog(ident, LOG_PID, LOG_LOCAL0);
}

void trace(LOG_LEVEL lev, const char *fmt, ...)
{
    va_list args;
    int sys_lev;
    if((lev > L_ERROR) && (_lev < lev))
        return;
    
    switch(lev)
    {
        case L_DEBUG_ERROR:
        case L_ERROR:
            sys_lev = LOG_ERR;
            break;
        case L_WARNING:
            sys_lev = LOG_WARNING;
            break;            
        case L_DEBUG:
            sys_lev = LOG_DEBUG;
            break;
        case L_NOTIFY:
            sys_lev = LOG_NOTICE;
            break;
        default:
            sys_lev = LOG_INFO;
            break;
    }
    
    va_start(args, fmt);
    vsyslog(sys_lev, fmt, args);
    
    va_end(args);
}


void trace(LOG_LEVEL lev, LOG_FACILITY facility, const char *fmt, ...) {
    
    va_list args;
    int sys_lev;
    if((lev > L_ERROR) && (_lev < lev) && !(facility & map_fac))
        return;
    
    switch(lev)
    {
        case L_DEBUG_ERROR:
        case L_ERROR:
            sys_lev = LOG_ERR;
            break;
        case L_WARNING:
            sys_lev = LOG_WARNING;
            break;            
        case L_DEBUG:
            sys_lev = LOG_DEBUG;
            break;
        case L_NOTIFY:
            sys_lev = LOG_NOTICE;
            break;
        default:
            sys_lev = LOG_INFO;
            break;
    }
    
    va_start(args, fmt);
    vsyslog(sys_lev, fmt, args);
    
    va_end(args);
}

