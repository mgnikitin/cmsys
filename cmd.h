/* 
 * File:   cmd.h
 * Author: user
 *
 * Created on 4 Октябрь 2015 г., 10:42
 */

#ifndef CMD_H
#define	CMD_H

enum TYPE_CMD {
    CMD_NONE    = 0x00,
    CMD_PING    = 0x01,
    CMD_PRES    = 0x02,
    CMD_ACCPT   = 0x03,
    CMD_SWON_L  = 0x04,
    CMD_SWOFF_L = 0x05,
    CMD_HALT    = 0x09,
    CMD_ALM_W   = 0x10,
    CMD_DCLN    = 0x12,
    CMD_TIME    = 0x13,
    CMD_IMAGE   = 0x14,
    CMD_REQ_ST	= 0x20,
    CMD_TEMPER  = 0x21,
    CMD_STATUS  = 0x22,
    CMD_DATA    = 0xF0
};

#endif	/* CMD_H */

