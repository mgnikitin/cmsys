#include "uartgate.h"
#include "trace.h"
#include <QDir>
#include <QFileInfo>

// ----------------------------------------------------------------------------
CUartGate::CUartGate(const CUart::t_uart_prm &prm)
{
    uart_prm = prm;
    uart = new CUart();
    connect(uart, SIGNAL(s_ready_read()), this, SLOT(slot_ready_read()));
    connect(uart, SIGNAL(s_exception()), this, SLOT(slot_device_exception()));
    connect(&prot, &CProt::s_new_cmd, this, &CUartGate::slot_new_cmd);
    connect(this, SIGNAL(s_parse(QByteArray)), &prot, SLOT(slot_parse(QByteArray)));

    QFileInfo fi(uart_prm.dev);
    fw.addPath(fi.absolutePath());
    connect(&fw, SIGNAL(directoryChanged(QString)), this, SLOT(slot_directory_changed(QString)));

    QTimer::singleShot(0, this, SLOT(ev_open()));
}

// ----------------------------------------------------------------------------
CUartGate::~CUartGate() {

    delete uart;
}

// ----------------------------------------------------------------------------
bool CUartGate::Open() {

    if(!uart->isOpen()) {
        if(!uart->Open(uart_prm.dev, uart_prm)) {
            trace(L_ERROR, QObject::trUtf8("CUartGate::Open(): Couldn't open COM-port '%1'").arg(uart_prm.dev).toLatin1());
            return false;
        }
        else {
            trace(L_NOTIFY, QObject::trUtf8("COM-port %1 (%2) succesfully opened")
                  .arg(uart_prm.dev).arg(uart_prm.baudrate).toLatin1());
            emit s_device_opened();
        }
    }
    return true;
}

// ----------------------------------------------------------------------------
void CUartGate::sendCmd(cmd_t cmd) {

    uint8_t arr_out[BUFFER_IN_SIZE];
    int len = prot.make_CMSMD(arr_out, cmd);
    if(!send_data(arr_out, len)) {
        trace(L_ERROR, "CUartGate: Couldn't write data");
    }
}

// ----------------------------------------------------------------------------
void CUartGate::ev_open() {

    if(QFileInfo::exists(uart_prm.dev)) {
        if(!Open()) {
            CEventMngr::instance().addEvent(this, &CUartGate::ev_open, 3600);
        }
    }
    else {
        trace(L_WARNING, QString("CUartGate: file %1 doesn't exist, wait until create").arg(uart_prm.dev).toLatin1());
    }
}

// ----------------------------------------------------------------------------
bool CUartGate::send_data(const uint8_t *arr, size_t len) {

    if(!uart->isOpen()) {
        trace(L_WARNING, "CUartGate: COM-port has not opened");
        return false;
    }
    trace(L_TRACE, QString("CUartGate::send_data() %1: %2")
          .arg(len).arg(QString(QByteArray((const char *)arr, len))).toLatin1());
    ssize_t sz = uart->write((const char *)arr, len);
    if(sz == -1)
        trace(L_ERROR, "CUartGate::send_data(): Couldn't write to COM-port: %s (%d)",
                strerror(errno), errno);
    return (sz == (ssize_t)len);
}

// ----------------------------------------------------------------------------
void CUartGate::slot_ready_read()
{
    QByteArray ba;
    uint8_t arr_in[BUFFER_IN_SIZE];
    ssize_t sz = uart->readAll((char *)&arr_in[0], BUFFER_IN_SIZE);

    if(sz == -1)
    {
        trace(L_ERROR, "CUartGate::slot_ready_read(): Couldn't read form uart %s (%d)", strerror(errno), errno);
        if(!uart->isOpen()) {
            trace(L_WARNING, QString("CUartGate::slot_ready_read(): try new open device").toLatin1());
            CEventMngr::instance().addEvent(this, &CUartGate::ev_open, 1000);
        }
    }
    else if(sz > 0)
    {
        ba.append((char *)&arr_in[0], sz);
        trace(L_TRACE, QString("CUartGate::slot_ready_read(): %1").arg(QString(ba)).toLatin1());
        emit s_parse(ba);
    }
}

// ----------------------------------------------------------------------------
void CUartGate::slot_device_exception() {

    trace(L_WARNING, QString("CUartGate::slot_device_exception(): try new open device").toLatin1());
    CEventMngr::instance().addEvent(this, &CUartGate::ev_open, 0);
}

// ----------------------------------------------------------------------------
void CUartGate::slot_new_cmd(cmd_t cmd) {

    trace(L_TRACE, "CUartGate: received new command");
    emit s_received_cmd(cmd);
}

// ----------------------------------------------------------------------------
void CUartGate::slot_directory_changed(QString dirname) {

    //trace(L_TRACE, QString("CUartGate: directory %1 changed").arg(dirname).toLatin1());
    QDir dir(dirname);
    if(dir.exists(uart_prm.dev) && uart) {
        //trace(L_NOTIFY, QString("CUartGate: %1 exists").arg(uart_prm.dev).toLatin1());
        if(!uart->isOpen()) {
            CEventMngr::instance().addEvent(this, &CUartGate::ev_open, 0);
        }
    }
}
