/* 
 * File:   config.h
 * Author: user
 *
 * Created on 28 Январь 2015 г., 22:42
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include "trace.h"
#include <string>

class Config;

/** \brief Параметры конфигурационного файла
  */
class t_config {
public:
    std::string uart_dev;       ///< \brief путь к устройству последовательного порта
    int uart_bd;                ///< \brief скорость последовательноо порта
    std::string db_name;        ///< \brief имя БД
    std::string db_host;        ///< \brief хост БД
    std::string db_user;        ///< \brief имя пользователя БД
    std::string db_password;    ///< \brief пароль пользователя БД
    std::string tg_token;
    std::string cam_host;
    std::string cam_user;
    std::string cam_password;
    unsigned char mk_addr;
    unsigned char addr_own;
    

private:

    /// \brief Конструктор
    t_config() {
        uart_bd = 9600;
    }

    friend class Config;
};

class Config {
public:

    static t_config defaultConfig();

    /** \brief Чтение конфигурационного файла
      */
    static bool readConfig(const char *filepath, t_config *cfg);
};

#endif	/* CONFIG_H */

