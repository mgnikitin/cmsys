#include "httpcamclient.h"

Q_LOGGING_CATEGORY(httpCamClient, "httpCamClient", QtWarningMsg)

// ----------------------------------------------------------------------------
HttpCamClient::HttpCamClient(QString host_, QString login_, QString passwrd_)
    : m_login(login_), m_passwrd(passwrd_), m_host(host_)
{

}

// ----------------------------------------------------------------------------
void HttpCamClient::requestImage() {

    sendRequest();
}

// ----------------------------------------------------------------------------
void HttpCamClient::sendRequest() {

    QNetworkReply *reply = generateRequest();
    m_mjpgState = MJPG_BOUNDARY;
    m_currentImageSize = 0;
    m_buffer.clear();
    m_image = QImage();
    if(reply) {
        connect(reply, &QIODevice::readyRead, this, &HttpCamClient::readJPEGData);
        connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
                this, &HttpCamClient::slotError);
        connect(reply, &QNetworkReply::sslErrors, this, &HttpCamClient::slotSslErrors);
    //    connect(reply, &QNetworkReply::finished, this, &HttpCamClient::onFinishedJPG);
        connect(reply, &QNetworkReply::finished, this, &HttpCamClient::onFinishedJPEG);
    }
    else {
        emit finished();
    }
}

// ----------------------------------------------------------------------------
QNetworkReply *HttpCamClient::generateRequest() {

    if(m_host.isEmpty()) { return nullptr; }
    QString url = QString::fromUtf8("http://%1:%2@%3/image/jpeg.cgi")
            .arg(m_login).arg(m_passwrd).arg(m_host);
    return qnam.get(QNetworkRequest(url));
}

// ----------------------------------------------------------------------------
int HttpCamClient::readBoundary(QIODevice &dev) {

    int img_sz = 0;
    bool quitNext = false;

    while(dev.canReadLine()) {
        QString cLine = dev.readLine();
        if (quitNext) {
            cLine = dev.readLine();
            break;
        }
        if (cLine.startsWith("Content-length:")) {
            bool ok;
            img_sz = cLine.mid(16).toInt(&ok);
            if (!ok) {
                qCWarning(httpCamClient) << QString("Could not convert %1 to number").arg(cLine.mid(16));
                return 0;
            }
            qCDebug(httpCamClient) << "Content-length " << img_sz;
        }
        else if (cLine.startsWith("Date:")) {
            m_mjpgState = MJPG_JPG;
            quitNext = true;
        }
        else {
            qCDebug(httpCamClient) << QString("%1").arg(cLine);
        }
    }

    return img_sz;
}

// ----------------------------------------------------------------------------
void HttpCamClient::readJPG(QIODevice &dev) {

    QByteArray b_arr = dev.read(m_currentImageSize - m_buffer.size());
    qCDebug(httpCamClient) << "Read bytes " << b_arr.size();
    m_buffer.append(b_arr);
    if (m_buffer.size() == m_currentImageSize) {
        m_image = QImage::fromData(m_buffer, "JPG");
        m_buffer.clear();
        if (m_image.format() == QImage::Format_Invalid) {
            qCWarning(httpCamClient) << "Image read fail";
        }
        m_mjpgState = MJPG_BOUNDARY;
    }
    else {
//          qCDebug(httpCamClient) << "Wait full data";
    }
}

// ----------------------------------------------------------------------------
void HttpCamClient::readMJPGData() {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);

    if(MJPG_BOUNDARY == m_mjpgState) {
         m_currentImageSize = readBoundary(*(static_cast<QIODevice *>(reply)));
    }
    if ((m_mjpgState == MJPG_JPG) && (m_currentImageSize > 0)) {
        readJPG(*(static_cast<QIODevice *>(reply)));
    }
}

// ----------------------------------------------------------------------------
void HttpCamClient::readJPEGData() {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);

    QByteArray b_arr = reply->readAll();
    qCDebug(httpCamClient) << "Read bytes " << b_arr.size();
    m_buffer.append(b_arr);
}

// ----------------------------------------------------------------------------
void HttpCamClient::onFinishedJPG() {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);

    reply->readAll();
    reply->deleteLater();
    emit finished();
}

// ----------------------------------------------------------------------------
void HttpCamClient::onFinishedJPEG() {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);

    reply->readAll();
    reply->deleteLater();
    m_image = QImage::fromData(m_buffer, "JPG");
    m_buffer.clear();
    if (m_image.format() == QImage::Format_Invalid) {
        qCWarning(httpCamClient) << "Image read fail";
    }
    emit finished();
}

// ----------------------------------------------------------------------------
void HttpCamClient::slotError(QNetworkReply::NetworkError) {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);
    qCWarning(httpCamClient) << QString("error: %1").arg(reply->errorString());
}

// ----------------------------------------------------------------------------
void HttpCamClient::slotSslErrors(const QList<QSslError> &lst) {

    QString str;
    for(QSslError error : lst) {
        str.append(QString("%1").arg(error.errorString()));
    }
    qCWarning(httpCamClient) << QString("ssl errors: %1").arg(str);
}
