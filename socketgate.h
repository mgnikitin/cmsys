#ifndef SOCKETGATE_H
#define SOCKETGATE_H

#include "gate.h"
#include <QTcpSocket>
#include "prot.h"

class CSocketGate : public CCommGate
{
    Q_OBJECT
public:
    CSocketGate(QTcpSocket *socket);

    virtual ~CSocketGate();

    void close();

    void sendCmd(cmd_t);

private:

    QTcpSocket *m_socket;
    CProt prot;

    bool send_data(const uint8_t *arr, size_t len);

private slots:

    void slot_ready_read();

    void slot_new_cmd(cmd_t);

signals:

    void s_parse(QByteArray);
};

#endif // SOCKETGATE_H
