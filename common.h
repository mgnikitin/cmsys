#ifndef COMMON_H
#define COMMON_H

#include "cmd.h"
#include <QUuid>
#include <QMetaType>
#include <QVariant>

typedef unsigned char uint8_t;
class CommandGenerator;

class cmd_t {
public:
    uint8_t addr_src;
    uint8_t addr_trg;
    TYPE_CMD cmd;
    TYPE_CMD resp_cmd;
    const QUuid uid;
    QVariant data;

private:

    cmd_t() : uid(QUuid::createUuid()) {}
    cmd_t(const QUuid &uid_) : uid(uid_) {}

    friend class CommandGenerator;
    friend class QtMetaTypePrivate::QMetaTypeFunctionHelper<cmd_t>;
};
Q_DECLARE_METATYPE(cmd_t)


class CommandGenerator {
public:

    static cmd_t generate(const uint8_t &addrSrc_,
                          const uint8_t &addrTrg_,
                          TYPE_CMD cmd_,
                          TYPE_CMD respCmd_ = CMD_NONE);

    static cmd_t generateReply(const uint8_t &addrSrc_, const cmd_t &cmd, TYPE_CMD t);
};

class StringCmdConv {
public:

    static QString reply(const cmd_t &c);

    static QString encode(TYPE_CMD t) {

        switch (t) {
        case CMD_NONE: return "none";
        case CMD_PING: return "ping";
        case CMD_PRES: return "present";
        case CMD_ACCPT: return "ok";
        case CMD_HALT: return "halt";
        case CMD_DCLN: return "decline";
        case CMD_TIME: return "time";
        case CMD_TEMPER: return "temper";
        case CMD_STATUS: return "status";
        case CMD_IMAGE: return "image";
        default: return "[undefined]";
        }
    }

    static TYPE_CMD decode(QString str) {

        if("status" == str) { return CMD_STATUS; }
        if("time" == str) { return CMD_TIME; }
        if("image" == str) { return CMD_IMAGE; }
        if("halt" == str)   { return CMD_HALT; }
        if("temper" == str) { return CMD_TEMPER; }
        return CMD_NONE;
    }

};

#endif // COMMON_H
