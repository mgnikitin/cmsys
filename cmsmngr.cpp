#include <QCoreApplication>
#include "cmsmngr.h"

// -----------------------------------------------------------------------------
CMSMngr::CMSMngr(const t_config &conf) : m_config(conf)
{
//    addr_own = conf.addr_own;
//    addr_mk = conf.mk_addr;
    
    need_pwr = false;
    curr_st = ST_CHECK_DEVICE;

    if(!bcm2835_init()) {
        trace(L_ERROR, QObject::trUtf8("CMSMngr::CMSMngr(): Couldn't init bcm2835").toLatin1());
    }

    init_devices(conf);

    proc = new QProcess(this);
    connect(proc, SIGNAL(readyReadStandardError()), this, SLOT(slot_read_proc_error()));
    connect(proc, SIGNAL(started()), this, SLOT(slot_proc_started()));
    connect(proc, SIGNAL(finished(int, QProcess::ExitStatus)),
            this, SLOT(slot_proc_finished(int, QProcess::ExitStatus)));
    connect(proc, SIGNAL(readyReadStandardOutput()),
            this, SLOT(slot_read_proc_stdout()));
}

// -----------------------------------------------------------------------------
CMSMngr::~CMSMngr()
{
    if(proc->state() != QProcess::NotRunning)
    {
        proc->close();
        if(!proc->waitForFinished(1000))
        {
            trace(L_WARNING, QObject::trUtf8("Kill process %1").arg(proc->pid()).toLatin1());
            proc->kill();
        }
        else
        {
            trace(L_NOTIFY, "Process succesfully closed");
        }
    }
    delete proc;

    detach_devices();

    bcm2835_close();
}

// -----------------------------------------------------------------------------
CCommGate *CMSMngr::add_gate(CCommGate *gate) {

    m_retr->addGate(gate);
    return gate;
}

// -----------------------------------------------------------------------------
void CMSMngr::init_devices(const t_config &conf) {
    
    qRegisterMetaType<cmd_t>("cmd_t");
    m_retr.reset(new CRetransl(m_config/*m_config.addr_own, db_prm*/));
    qRegisterMetaType<uint8_t>("uint8_t");

    connect(m_retr.data(), &CRetransl::s_present_device, this, &CMSMngr::slot_present_device, Qt::QueuedConnection);
    connect(m_retr.data(), &CRetransl::s_halt_request, this, &CMSMngr::s_close);

    CUart::t_uart_prm uart_prm;

    uart_prm.dev = QString::fromStdString(conf.uart_dev);
    if(9600 == conf.uart_bd)
        uart_prm.baudrate = BD9600;
    else if(57600 == conf.uart_bd)
        uart_prm.baudrate = BD57600;
    else
        uart_prm.baudrate = BD9600;
    uart_prm.data = D8;
    uart_prm.flow = FLOW_NONE;
    uart_prm.parity = NO_PAR;

    add_gate(new CUartGate(uart_prm));
    add_gate(new CUdpSocketGate(3434));
    add_gate(new TgGate(m_config.addr_own, QString::fromStdString(conf.tg_token)));
    
    tcp_server = new CTcpServer(m_config.addr_own);
    connect(tcp_server, &CTcpServer::s_received_cmd, m_retr.data(), static_cast<void (CRetransl::*)(QUuid, cmd_t)>(&CRetransl::receivedCmd), Qt::QueuedConnection);
    connect(m_retr.data(), &CRetransl::s_send_cmd, tcp_server, &CTcpServer::slot_send_cmd, Qt::QueuedConnection);
    tcp_server->Open();
}

// -----------------------------------------------------------------------------
void CMSMngr::detach_devices() {
    
    m_retr.reset(nullptr);

    tcp_server->Close();
    delete tcp_server;
}

// -----------------------------------------------------------------------------
void CMSMngr::start()
{
    curr_st = ST_START; //ST_CHECK_DEVICE;
    exec_state();
}

// -----------------------------------------------------------------------------
void CMSMngr::exec_state()
{
    switch(curr_st)
    {
    case ST_CHECK_DEVICE:
        trace(L_DEBUG, "curr state: check GSM modem power");
        proc->start("modempower check");
        if(!proc->waitForStarted(2000))
        {
            trace(L_DEBUG, "Process waiting time is over or error occurred");
        }
        else
        {
            trace(L_DEBUG, "Process successfully started");
        }
    break;
    case ST_POWER_UP_DEVICE:
        trace(L_DEBUG, "curr state: set GSM modem power");
        proc->start("modempower powerup");
        if(!proc->waitForStarted(2000))
        {
            trace(L_DEBUG, "Process waiting time is over or error occurred");
        }
        else
        {
            trace(L_DEBUG, "Process successfully started");
        }
    break;
    case ST_START:
        trace(L_DEBUG, "curr state: start");
    break;
    default:
        trace(L_WARNING, QObject::trUtf8("CMSMngr::exec_state(): undefined state: %1")
                .arg((int)curr_st).toLatin1());
    break;
    }
}

// -----------------------------------------------------------------------------
void CMSMngr::cmd_halt()
{
    trace(L_DEBUG, QObject::trUtf8("cmd halt").toLatin1());
    proc->start("halt");
    if(!proc->waitForStarted(2000))
    {
        trace(L_DEBUG, "Process waiting time is over or error occurred");
    }
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_quit()
{
    if(m_retr)
        m_retr->close();
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_read_proc_error()
{
    trace(L_ERROR, proc->readAllStandardError());
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_proc_started()
{
    trace(L_DEBUG, QObject::trUtf8("Process %1 started").arg(proc->pid()).toLatin1());
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_proc_finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    trace(L_NOTIFY, QObject::trUtf8("Process finished with code %1").arg(exitCode).toLatin1());
    if(ST_CHECK_DEVICE == curr_st)
    {
        curr_st = need_pwr ? ST_POWER_UP_DEVICE : ST_START;
        exec_state();
    }
    else if(ST_POWER_UP_DEVICE == curr_st)
    {
        if(QProcess::NormalExit == exitStatus)
        {
            curr_st = ST_START;
            exec_state();
        }
        else
        {
            trace(L_WARNING, QObject::trUtf8("process is crashed").toLatin1());
       //     slot_halt();
            QCoreApplication::exit(0);
        }
    }
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_read_proc_stdout()
{
    QByteArray ba = proc->readAllStandardOutput();
    trace(L_DEBUG, QString(ba).toLatin1());
    if(ST_CHECK_DEVICE == curr_st)
    {
        if(QByteArray("device is turned on\n") == ba)
        {
            trace(L_DEBUG, "need power false");
            need_pwr = false;
        }
        else if(QByteArray("device is turned off\n") == ba)
        {
            need_pwr = true;
            trace(L_DEBUG, "need power true");
        }
    }
}

// -----------------------------------------------------------------------------
void CMSMngr::slot_present_device(uint8_t addr)
{
    trace(L_NOTIFY, QString("Device %1 present").arg(addr, 2, 16, QChar('0')).toLatin1());
}
