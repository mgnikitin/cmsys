#include <QtCore/qobject.h>

#include "tcp_server.h"
#include "trace.h"

// ----------------------------------------------------------------------------
CTcpServer::CTcpServer(uint8_t addr_own)
    : address_own(addr_own)
{
    
    tcp_server = new QTcpServer();
    connect(tcp_server, &QTcpServer::newConnection, this, &CTcpServer::slot_new_connection);
}

// ----------------------------------------------------------------------------
CTcpServer::~CTcpServer() {
    
    Close();
    delete tcp_server;
}

// ----------------------------------------------------------------------------
bool CTcpServer::Open() {
    
    if(!tcp_server) {
        trace(L_ERROR, QObject::trUtf8("CTcpServer::Open(): tcp-server is not created").toLatin1());
        return false;
    }
    
    if(tcp_server->listen(QHostAddress::Any, 3434)) {
        trace(L_NOTIFY, QObject::trUtf8("tcp-server started on port %1").arg(3434).toLatin1());
    }
    else {
        trace(L_WARNING, QObject::trUtf8("Couldn't start tcp-server %1").arg(tcp_server->errorString()).toLatin1());
        return false;
    }
    return true;
}

// ----------------------------------------------------------------------------
bool CTcpServer::Close() {
    
    QMap<QUuid, CSocketGate *>::const_iterator iter = map_socket.constBegin();
    while(iter != map_socket.constEnd()) {
        iter.value()->close();
        iter++;
    }
    map_socket.clear();
    
    if(tcp_server) {
        tcp_server->close();
        return true;
    }
    
    return true;
}

// ----------------------------------------------------------------------------
void CTcpServer::slot_send_cmd(QUuid id, cmd_t cmd) {
    
    CSocketGate *gate = map_socket.value(id, NULL);
    if(gate)
        gate->slot_send_cmd(id, cmd);
}

// ----------------------------------------------------------------------------
void CTcpServer::slot_new_connection() {
    
    QTcpSocket *tcp_socket = tcp_server->nextPendingConnection();
    
    trace(L_DEBUG, QObject::trUtf8("CTcpServer: append new socket").toLatin1());
    CSocketGate *gate = new CSocketGate(tcp_socket);
    connect(gate, &CSocketGate::s_received_cmd, this, &CTcpServer::slot_received_cmd);
    map_socket.insert(gate->GetID(), gate);
}


// ----------------------------------------------------------------------------
void CTcpServer::slot_received_cmd(cmd_t cmd) {

    CCommGate *gate = dynamic_cast<CCommGate *>(sender());

    if(!gate) {
        trace(L_ERROR, "CTcpServer::slot_received_cmd(): Couldn't dynamic cast");
        return;
    }

    emit s_received_cmd(gate->GetID(), cmd);
}

