/* 
 * File:   retransl.h
 * Author: user
 *
 * Created on 7 Февраль 2015 г., 12:26
 */
 
#ifndef RETRANSL_H
#define	RETRANSL_H

#include <QObject>
#include "tcp_server.h"
#include "cmd.h"
#include "exec_thread.h"
#include "db_inter.h"
#include "httpcamclient.h"
#include "config.h"

class HttpCamRequest : public HttpCamClient {
    Q_OBJECT
public:
    QUuid id;
    cmd_t c;

    HttpCamRequest(const QUuid &id_, const cmd_t &cmd_, const t_config &cfg_)
        : HttpCamClient(QString::fromStdString(cfg_.cam_host),
                        QString::fromStdString(cfg_.cam_user),
                        QString::fromStdString(cfg_.cam_password)), id(id_), c(cmd_) {}
};

Q_DECLARE_LOGGING_CATEGORY(retransl)

class CRetransl : public QObject {
    Q_OBJECT
public:
    
    CRetransl(const t_config &cfg_);

    virtual ~CRetransl();

    void addGate(CCommGate *gate);

    void sendModeReply(QUuid id, cmd_t, TYPE_CMD);
    void sendDataReply(QUuid id, cmd_t cmd, TYPE_CMD t, const QVariant &dat = QVariant());
    
    void gen_present(uint8_t addr);

    void close();
    
private:

    const uint8_t addr_own;
    const t_config m_config;
    CExecThread *db_thread; ///< \brief Класс потока БД
    DBInter * db_inter;     ///< \brief Класс взаимодействия с БД
    QMap<uint8_t, QUuid> map_addr;
    QMap<QUuid, CCommGate *> map_gate;

    void init_db(const DBInter::db_prm_t &conf);

    void send_broadcast(QUuid id, cmd_t c);

    void close_db();

    void ev_connectDB();

    void receivedSelfCmd(QUuid id, cmd_t c);
    void receivedOtherCmd(QUuid id, cmd_t c);
    void receivedBroadcastCmd(QUuid id, cmd_t c);

public slots:

    void receivedCmd(cmd_t c);
    void receivedCmd(QUuid id, cmd_t c);

private slots:

    void onGetImage();
    
signals:

    void s_present_device(uint8_t);

    void s_send_cmd(QUuid, cmd_t);

    void s_connect_db();

    void s_disconnect_db();

    void s_halt_request();
};

#endif	/* RETRANSL_H */

