/** \file queuebuffer.cpp
  * \brief Статический буфер типа очередь (FIFO)
  * \author Никитин М. Г., отд. 023
  * \version 1.1
  */
#include "queuebuf.h"


CQueueBuffer::CQueueBuffer(const unsigned short &size) : sz(size)
{
    offset_set = 0;
    offset_get = 0;
    _count = 0;
    arr = new uint8_t[sz];
}

CQueueBuffer::~CQueueBuffer()
{
    delete [] arr;
}

bool CQueueBuffer::set(const uint8_t &b)
{
    if(_count < sz)
    {
        arr[offset_set] = b;
        move_right(offset_set);
        _count++;
        return true;
    }

    return false;
}

unsigned short CQueueBuffer::set(uint8_t *first, unsigned short count)
{
    unsigned short passed = 0;
    if(count == 0)
        return 0;

    do
    {
        if(!set(first[passed]))
            break;

        passed++;
    }
    while(passed < count);

    return passed;
}

bool CQueueBuffer::get(uint8_t &b)
{
    if(_count > 0)
    {
        b = arr[offset_get];
        move_right(offset_get);
        _count--;
        return true;
    }

    return false;
}

void CQueueBuffer::get_attrib(unsigned short &count, unsigned short &offst_get) const
{
    count = _count;
    offst_get = offset_get;
}

void CQueueBuffer::set_attrib(const unsigned short &count, const unsigned short &offst_get)
{
    _count = count;
    offset_get = offst_get;
}

bool CQueueBuffer::find(uint8_t *arr, unsigned int size)
{
    uint8_t b;
    // результат
    bool rez = false;
    // номер элемента для сравнения
    unsigned short pos_comp = 0;

    if(size == 0)
        return rez;

    bool has_saved = false;
    // сохраненные атрибуты буфера
    unsigned short save_count = 0;
    unsigned short save_offset = 0;
    // атрибуты буфера
    unsigned short rez_cnt, rez_offset;

    get_attrib(rez_cnt, rez_offset);

    while(get(b))
    {
        // сравниваем байты буфера с массивом
        if(b == arr[pos_comp])
        {
            pos_comp++;
        }
        else
        {   // байты не совпали
            // начинаем поиск сначала
            pos_comp = 0;

            // пытаемся восстановить сохраненное состояние атрибутов
            if(has_saved)
                set_attrib(save_count, save_offset);

            // обновляем атрибуты
            get_attrib(rez_cnt, rez_offset);

            // сохраненные атрибуты больше не нужны, повторяем попытку
            if(has_saved)
            {
                has_saved = false;
                continue;
            }
        }

        // если массивы совпали, восстанавливаем атрибуты и выходим
        if(pos_comp >= size)
        {
            rez = true;
            break;
        }

        // сохраняем состояние атрибутов
        if(pos_comp == 1) {
            get_attrib(save_count, save_offset);
            has_saved = true;
        }
    }
    
    set_attrib(rez_cnt, rez_offset);

    return rez;
}

bool CQueueBuffer::silent_find(uint8_t *arr, unsigned int size)
{
    // результат
    bool rez = false;
    // сохраненные атрибуты буфера
    unsigned short save_count = 0;
    unsigned short save_offset = 0;

    // сохраняем атрибуты буфера
    get_attrib(save_count, save_offset);
    // производим поиск
    rez = find(arr, size);
    // восстанавливаем атрибуты
    set_attrib(save_count, save_offset);
    // возвращаем результат
    return rez;
}

unsigned short CQueueBuffer::used_count() const
{
    return _count;
}

unsigned short CQueueBuffer::avail_count() const
{
    if(sz > _count)
        return sz - _count;

    return 0;
}

unsigned short CQueueBuffer::size() const
{
    return sz;
}

void CQueueBuffer::move_right(unsigned short &offset)
{
    unsigned short _sz = (sz == 0) ? 0 : (unsigned short)(sz-1);
    if(offset >= _sz)
        offset = 0;
    else
        offset++;
}

