#ifndef CMSMNGR_H
#define CMSMNGR_H

#include <QObject>
#include <bcm2835.h>
#include "trace.h"
#include "uartgate.h"
#include "udpsocketgate.h"
#include "tggate.h"
#include "config.h"
#include "retransl.h"
#include "eventmngr.h"

enum STATES
{
    ST_CHECK_DEVICE,
    ST_POWER_UP_DEVICE,
    ST_START
};

class CMSMngr : public QObject
{
    Q_OBJECT
public:
    CMSMngr(const t_config &conf);
    virtual ~CMSMngr();

    void start();

private:

    CTcpServer *tcp_server;

    QScopedPointer<CRetransl> m_retr;        ///< \brief Класс обмена с микроконтроллером
    const t_config m_config;
//    uint8_t addr_mk;
//    uint8_t addr_own;

    QProcess *proc;
    STATES curr_st;
    bool need_pwr;

    QString req_numbver;

    CCommGate *add_gate(CCommGate *gate);

    void init_devices(const t_config &conf);
    
    void detach_devices();
    
    void cmd_halt();

    void exec_state();

private slots:

    void slot_quit();

    void slot_read_proc_error();
    void slot_proc_started();
    void slot_proc_finished(int exitCode, QProcess::ExitStatus exitStatus);
    void slot_read_proc_stdout();

    void slot_present_device(uint8_t);

signals:

    void s_close();

};

#endif // CMSMNGR_H
