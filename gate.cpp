#include "gate.h"

// ----------------------------------------------------------------------------
CCommGate::CCommGate() : uuid(QUuid::createUuid())
{
}

// ----------------------------------------------------------------------------
CCommGate::~CCommGate() {

}

// ----------------------------------------------------------------------------
void CCommGate::slot_send_cmd(QUuid id, cmd_t cmd) {

    if(id != this->GetID())
        return;

    sendCmd(cmd);
}
