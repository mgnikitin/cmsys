/* 
 * File:   tcp_server.h
 * Author: user
 *
 * Created on 10 Октябрь 2015 г., 14:43
 */

#ifndef TCP_SERVER_H
#define	TCP_SERVER_H
 
#include <QTcpServer>
#include <QTcpSocket>
#include <QObject>
#include <QUuid>
#include <QMap>
#include "socketgate.h"

class CTcpServer : public QObject {
    
    Q_OBJECT
public:
    CTcpServer(uint8_t addr_own);

    virtual ~CTcpServer();
    
    bool Open();
    
    bool Close();
private:

    QTcpServer *tcp_server;
    
    QMap<QUuid, CSocketGate *> map_socket;

    uint8_t address_own;
    
public slots:
        
    void slot_send_cmd(QUuid, cmd_t);
    
private slots:

    void slot_new_connection();

    void slot_received_cmd(cmd_t);

signals:

    void s_received_cmd(QUuid, cmd_t);
};

#endif	/* TCP_SERVER_H */

