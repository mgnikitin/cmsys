/* 
 * File:   retransl.cpp
 * Author: user
 * 
 * Created on 7 Февраль 2015 г., 12:26
 */

#include "retransl.h"

Q_LOGGING_CATEGORY(retransl, "retransl", QtWarningMsg)

// ----------------------------------------------------------------------------
CRetransl::CRetransl(const t_config &cfg_)
: addr_own(cfg_.addr_own), m_config(cfg_)

{
    DBInter::db_prm_t db_prm;
    db_prm.db_name = QString::fromStdString(cfg_.db_name);
    db_prm.db_host = QString::fromStdString(cfg_.db_host);
    db_prm.db_user = QString::fromStdString(cfg_.db_user);
    db_prm.db_password = QString::fromStdString(cfg_.db_password);

    db_thread = NULL;
    db_inter = NULL;

    init_db(db_prm);
}

// ----------------------------------------------------------------------------
CRetransl::~CRetransl() {
    
    close_db();
}

// ----------------------------------------------------------------------------
void CRetransl::addGate(CCommGate *gate) {

    connect(gate, &CCommGate::s_received_cmd, this, static_cast<void (CRetransl::*)(cmd_t)>(&CRetransl::receivedCmd), Qt::QueuedConnection);
    connect(this, &CRetransl::s_send_cmd, gate, &CCommGate::slot_send_cmd, Qt::QueuedConnection);
    map_gate.insert(gate->GetID(), gate);
}

// ----------------------------------------------------------------------------
void CRetransl::sendModeReply(QUuid id, cmd_t cmd, TYPE_CMD t)
{
    cmd_t c = CommandGenerator::generateReply(addr_own, cmd, t);
    emit s_send_cmd(id, c);
}

// ----------------------------------------------------------------------------
void CRetransl::sendDataReply(QUuid id, cmd_t cmd, TYPE_CMD t, const QVariant &dat) {

    cmd_t c = CommandGenerator::generateReply(addr_own, cmd, t);
    c.data = dat;
    emit s_send_cmd(id, c);
}

// ----------------------------------------------------------------------------
void CRetransl::gen_present(uint8_t addr)
{
    emit s_present_device(addr);
}

// ----------------------------------------------------------------------------
void CRetransl::close() {

    close_db();
}

// -----------------------------------------------------------------------------
void CRetransl::init_db(const DBInter::db_prm_t &conf) {

    db_thread = new CExecThread();
    db_inter = new DBInter(addr_own, conf);
    db_inter->moveToThread(db_thread);

    db_thread->start();

    connect(this, SIGNAL(s_connect_db()), db_inter, SLOT(slot_connect()));
    connect(this, SIGNAL(s_disconnect_db()), db_inter, SLOT(slot_disconnect()));
    connect(this, SIGNAL(s_send_cmd(QUuid, cmd_t)), db_inter, SLOT(slot_response_cmd(QUuid, cmd_t)));
    connect(db_inter, &DBInter::s_req_cmd, this, static_cast<void (CRetransl::*)(QUuid, cmd_t)>(&CRetransl::receivedCmd), Qt::QueuedConnection);

    CEventMngr::instance().addEvent(this, &CRetransl::ev_connectDB, 20);
}

// -----------------------------------------------------------------------------
void CRetransl::close_db() {

    qCDebug(retransl) << QString::fromUtf8("close_db()");
    if(db_thread) {
        if(db_thread->isRunning())
        {
            db_thread->quit();
            if(!db_thread->wait(2000))
            {
                qCWarning(retransl) << QString::fromUtf8("Terminating process");
                db_thread->terminate();
            }
        }
        delete db_thread;
        db_thread = NULL;
    }
    if(db_inter) {
        delete db_inter;
        db_inter = NULL;
    }
}

// -----------------------------------------------------------------------------
void CRetransl::ev_connectDB() {

    emit s_connect_db();
}

// ----------------------------------------------------------------------------
void CRetransl::send_broadcast(QUuid id, cmd_t c) {

    QMap<QUuid, CCommGate *>::const_iterator iter = map_gate.begin();
    while(iter != map_gate.end()) {
        CCommGate *gate = iter.value();
        if(gate->GetID() != id) {
            gate->sendCmd(c);
        }

        iter++;
    }
}

// ----------------------------------------------------------------------------
void CRetransl::receivedSelfCmd(QUuid id, cmd_t c) {

    qCDebug(retransl) << QString::fromUtf8("%1 request from 0x%2")
          .arg(StringCmdConv::encode(c.cmd)).arg(c.addr_src, 2, 16, QChar('0'));
    switch (c.cmd) {
    case CMD_HALT:
        emit s_halt_request();
        break;
    case CMD_PING:
        sendModeReply(id, c, CMD_PRES);
        break;
    case CMD_PRES:
        gen_present(c.addr_src);
        break;
    case CMD_TEMPER:
        sendDataReply(id, c, CMD_ACCPT, QVariant::fromValue(25)); // TODO
        break;
    case CMD_STATUS:
        sendModeReply(id, c, CMD_ACCPT);
    break;
    case CMD_TIME:
        sendDataReply(id, c, CMD_ACCPT, QVariant::fromValue(QDateTime::currentDateTime()));
    break;
    case CMD_IMAGE:
    {
        HttpCamClient *httpReq = new HttpCamRequest(id, c, m_config);
        connect(httpReq, &HttpCamClient::finished, this, &CRetransl::onGetImage);
        CEventMngr::instance().addEvent<HttpCamClient>(httpReq, &HttpCamClient::requestImage, 1);
    }
    break;
    default:
    break;
    }
}

// ----------------------------------------------------------------------------
void CRetransl::receivedOtherCmd(QUuid id, cmd_t c) {

    qCDebug(retransl) << QString::fromUtf8("id %1").arg(id.toString());
    // ищем устройство обмена, связанное с адресатом
    if(map_addr.contains(c.addr_trg)) {
        QUuid id = map_addr.value(c.addr_trg);
        emit s_send_cmd(id, c);
    }
    else {
        // рассылаем сообщение всем абонентам, кроме отправителя
        send_broadcast(id, c);
    }
}

// ----------------------------------------------------------------------------
void CRetransl::receivedBroadcastCmd(QUuid id, cmd_t c) {

    qCDebug(retransl) << QString::fromUtf8("id %1").arg(id.toString());
    // рассылаем сообщение всем абонентам, кроме отправителя
    send_broadcast(id, c);
}

// ----------------------------------------------------------------------------
void CRetransl::receivedCmd(QUuid id, cmd_t c) {

    qCDebug(retransl) << QString::fromUtf8("id %1 from 0x%2").arg(id.toString())
                         .arg(c.addr_src, 2, 16, QChar('0'));
    if((c.addr_trg == addr_own) || (0xFF == c.addr_trg)) {
        map_addr.insert(c.addr_src, id);    // связываем абонента с устройством обмена
        receivedSelfCmd(id, c);
    }

    if((c.addr_trg != addr_own) && (0xFF != c.addr_trg)) {
        map_addr.insert(c.addr_src, id);    // связываем абонента с устройством обмена
        receivedOtherCmd(id, c);
    }

    if((c.addr_trg != addr_own) && (0xFF == c.addr_trg)) {
        receivedBroadcastCmd(id, c);
    }
}

// ----------------------------------------------------------------------------
void CRetransl::receivedCmd(cmd_t c) {

    CCommGate *gate = dynamic_cast<CCommGate *>(sender());
    if(!gate) {
        qCWarning(retransl) << QString::fromUtf8("Couldn't dynamic cast");
        return;
    }
    QUuid id = gate->GetID();

    receivedCmd(id, c);
}

// ----------------------------------------------------------------------------
void CRetransl::onGetImage() {

    HttpCamRequest *httpReq = dynamic_cast<HttpCamRequest *>(sender());
    Q_ASSERT(httpReq);
    if(QImage::Format_Invalid != httpReq->image().format()) {
        sendDataReply(httpReq->id, httpReq->c, CMD_ACCPT, QVariant::fromValue(httpReq->image()));
    }
    else {
        sendDataReply(httpReq->id, httpReq->c, CMD_DCLN);
    }
    httpReq->deleteLater();
}
