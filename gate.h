#ifndef GATE_H
#define GATE_H

#include <QObject>
#include <QUuid>
#include <QLoggingCategory>
#include "eventmngr.h"
#include "cmd.h"
#include "queuebuf.h"
#include "trace.h"

class CCommGate : public QObject
{
    Q_OBJECT
public:
    CCommGate();

    virtual ~CCommGate();

    const QUuid &GetID() const { return uuid; }

    virtual void sendCmd(cmd_t) = 0;

protected:

    const QUuid uuid;

private:

    Q_DISABLE_COPY(CCommGate)

public slots:

    void slot_send_cmd(QUuid id, cmd_t cmd);

signals:

    void s_received_cmd(cmd_t);
};

#endif // GATE_H
