/* 
 * File:   eventmngr.cpp
 * Author: user
 * 
 * Created on 7 Февраль 2015 г., 16:21
 */

#include "eventmngr.h"

Q_LOGGING_CATEGORY(eventHandler, "eventHandler", QtWarningMsg)
Q_LOGGING_CATEGORY(eventMngr, "eventMngr", QtWarningMsg)

CEventMngr::CEventMngr() {
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(bypass_events()));
    startEvents();
}

CEventMngr::~CEventMngr() {
    delete timer;
}

void CEventMngr::startEvents()
{
    timer->start(1000);
}

void CEventMngr::insert(const CEventMngr::t_event &ev) {
    
//    for(int i =0; events.count(); i++) { TODO
//        if(events.at(i).ev_handlr == ev.ev_handlr) {
//            events[i] = ev;
//            return;
//        }
//    }
    events.append(ev);
}

bool CEventMngr::remove(CEventHandlerBase *evh) {
    
    for(int i =0; events.count(); i++) {
        if(events.at(i).ev_handlr == evh) {
            events.removeAt(i);
            return true;
        }
    }
    qCWarning(eventMngr) << QString("couldn't find");
    return false;
}

void CEventMngr::bypass_events()
{
    bypass = true;
    
    for(int i = 0; i < events.count(); )
    {
        if(events.at(i).updated) {
            events[i].updated = false;
            i++;
            continue;
        }
        if(events.at(i).interval <= 1)
        {
            CEventHandlerBase *evh = events.at(i).ev_handlr;
            remove(evh);
            evh->execute();
            continue;
        }
        else {
            events[i].interval--;
        }
        i++;
    }
    bypass = false;
}
