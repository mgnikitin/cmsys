#include "socketgate.h"
#include "queuebuf.h"

// ----------------------------------------------------------------------------
CSocketGate::CSocketGate(QTcpSocket *socket)
    : m_socket(socket)
{
    QObject::connect(m_socket, SIGNAL(disconnected()), m_socket, SLOT(deleteLater()));
    QObject::connect(m_socket, SIGNAL(readyRead()), this, SLOT(slot_ready_read()));

    connect(&prot, &CProt::s_new_cmd, this, &CSocketGate::slot_new_cmd);
    connect(this, SIGNAL(s_parse(QByteArray)), &prot, SLOT(slot_parse(QByteArray)));
}

// ----------------------------------------------------------------------------
CSocketGate::~CSocketGate() {

}

// ----------------------------------------------------------------------------
bool CSocketGate::send_data(const uint8_t *arr, size_t len) {

    if(!m_socket->isOpen())
        return false;
    ssize_t sz = m_socket->write((const char *)arr, len);
    if(sz == -1) {
        trace(L_ERROR, "CSocketGate::send_data(): Couldn't write to socket:");
        return false;
    }
    trace(L_TRACE, "CSocketGate: Send data to socket");
    return (sz == (ssize_t)len);
}

// ----------------------------------------------------------------------------
void CSocketGate::slot_ready_read() {

    trace(L_TRACE, QObject::trUtf8("CTcpServer::slot_ready_read()").toLatin1());
    QTcpSocket* client_socket = (QTcpSocket*)sender();
//    int id = client_socket->socketDescriptor();
    QByteArray ba = client_socket->readAll();
    if(ba.size() > 0)
    {
        emit s_parse(ba);
    }
}

// ----------------------------------------------------------------------------
void CSocketGate::close() {

    if(m_socket)
        m_socket->close();
}

// ----------------------------------------------------------------------------
void CSocketGate::sendCmd(cmd_t cmd) {

    uint8_t arr_out[BUFFER_IN_SIZE];
    int len = prot.make_CMSMD(arr_out, cmd);
    send_data(arr_out, len);
    m_socket->disconnectFromHost();
}

// ----------------------------------------------------------------------------
void CSocketGate::slot_new_cmd(cmd_t cmd) {

    trace(L_TRACE, "CSocketGate: received new command");
    emit s_received_cmd(cmd);
}
