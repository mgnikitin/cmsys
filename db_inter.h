#ifndef DB_INTER_H
#define DB_INTER_H

#include <QObject>
#include <QVariant>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDateTime>
#include <QTimer>
#include <QProcess>
#include <QUuid>

#include "cmd.h"
#include "common.h"

class DBInter : public QObject
{
    Q_OBJECT
public:

    struct db_prm_t {
        QString db_name;
        QString db_host;
        QString db_user;
        QString db_password;
    };
    
    DBInter(uint8_t addr, db_prm_t cfg);

    virtual ~DBInter();

private:

    uint8_t addr_own;

    QSqlDatabase db;
    bool f_stop;

    const QUuid id;

    db_prm_t db_cfg;
    
    QDateTime dt_fr; ///< время с которого запрашиваются записи из БД
    QMap<uint8_t, QString> map_number;

    void db_query_field_names();
    void db_query();

    QString cmd_to_query(cmd_t cmd);

    void parse_cmd(QString number, QString str);

private slots:

    void slot_connect();
    void slot_disconnect();
    void slot_db_query();
    
    void slot_response_cmd(QUuid id, cmd_t cmd);

signals:
    
    void s_req_cmd(QUuid, cmd_t);
};

#endif // DB_INTER_H
