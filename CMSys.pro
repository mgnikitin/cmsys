
TEMPLATE = app
TARGET = cmsysd
VERSION = 1.0.0
CONFIG += c++11

QMAKE_CFLAGS_RELEASE += -g
QMAKE_CXXFLAGS_RELEASE += -g
DEFINES += QT_MESSAGELOGCONTEXT

QT -= gui
QT += core sql network gui

SOURCES +=  cmsmngr.cpp \
            config.cpp \
            daemonapp.cpp \
            db_inter.cpp \
            eventmngr.cpp \
            exec_thread.cpp \
            main.cpp \
            queuebuf.cpp \
            retransl.cpp \
            trace.cpp \
            uart.cpp \
    tcp_server.cpp \
    gate.cpp \
    uartgate.cpp \
    socketgate.cpp \
    prot.cpp \
    udpsocketgate.cpp \
    tggate.cpp \
    common.cpp \
    httpcamclient.cpp
HEADERS +=  cmsmngr.h \
            config.h \
            daemonapp.h \
            db_inter.h \
            eventmngr.h \
            exec_thread.h \
            queuebuf.h \
            retransl.h \
            trace.h \
            uart.h \
    tcp_server.h \
    cmd.h \
    gate.h \
    uartgate.h \
    socketgate.h \
    prot.h \
    common.h \
    udpsocketgate.h \
    tggate.h \
    httpcamclient.h

LIBS += -lbcm2835
