#ifndef UARTGATE_H
#define UARTGATE_H

#include "gate.h"
#include "uart.h"
#include "prot.h"
#include <QFileSystemWatcher>

class CUartGate : public CCommGate
{
    Q_OBJECT
public:
    CUartGate(const CUart::t_uart_prm &prm);

    virtual ~CUartGate();

    bool Open();

    void sendCmd(cmd_t);

private:

    CUart::t_uart_prm uart_prm;
    CUart   *uart;          ///< \brief Класс взаимодействия с последовательным портом
    CProt prot;

    QFileSystemWatcher fw;

    bool send_data(const uint8_t *arr, size_t len);

private slots:

    void slot_ready_read();

    void slot_device_exception();

    void slot_new_cmd(cmd_t);

    void ev_open();

    void slot_directory_changed(QString filename);

signals:

    void s_parse(QByteArray);

    void s_device_opened();
};

#endif // UARTGATE_H
