#ifndef HTTPCAMCLIENT_H
#define HTTPCAMCLIENT_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslError>
#include <QBuffer>
#include <QLoggingCategory>
#include <QImage>

Q_DECLARE_LOGGING_CATEGORY(httpCamClient)

class HttpCamClient : public QObject
{
    Q_OBJECT
public:
    HttpCamClient(QString host_, QString login_, QString passwrd_);

    void requestImage();
    const QImage &image() const { return m_image; }

private:

    enum MJPG_STATE {
        MJPG_BOUNDARY,
        MJPG_JPG
    };

    QNetworkAccessManager qnam;
    MJPG_STATE m_mjpgState;
    int m_currentImageSize;
    QByteArray m_buffer;
    QImage m_image;
    QString m_login;
    QString m_passwrd;
    QString m_host;

    void sendRequest();
    QNetworkReply *generateRequest();

    int readBoundary(QIODevice &dev);
    void readJPG(QIODevice &dev);

private slots:

    void readMJPGData();
    void readJPEGData();
    void onFinishedJPG();
    void onFinishedJPEG();
    void slotError(QNetworkReply::NetworkError);
    void slotSslErrors(const QList<QSslError> &lst);

signals:

    void finished();
};

#endif // HTTPCAMCLIENT_H
