#include "common.h"
#include <QDateTime>

// ----------------------------------------------------------------------------
cmd_t CommandGenerator::generate(const uint8_t &addrSrc_, const uint8_t &addrTrg_, TYPE_CMD cmd_, TYPE_CMD respCmd_) {

    cmd_t c;
    c.addr_src = addrSrc_;
    c.addr_trg = addrTrg_;
    c.cmd = cmd_;
    c.resp_cmd = respCmd_;

    return c;
}

// ----------------------------------------------------------------------------
cmd_t CommandGenerator::generateReply(const uint8_t &addrSrc_, const cmd_t &cmd, TYPE_CMD t) {

    cmd_t c(cmd.uid);
    c.addr_src = addrSrc_;
    c.addr_trg = cmd.addr_src;
    c.resp_cmd = cmd.cmd;
    c.cmd = t;

    return c;
}

// ----------------------------------------------------------------------------
QString StringCmdConv::reply(const cmd_t &c) {

    if(CMD_ACCPT == c.cmd) {
        switch (c.resp_cmd) {
        case CMD_TIME:
            return QString("time %1").arg(c.data.toDateTime().toString("hh:mm:ss"));
            break;
        case CMD_TEMPER:
            return QString("temper %1").arg(c.data.toDouble());
            break;
        default:
            return encode(c.cmd);
            break;
        }
    }
    else {
        return QString("%1").arg(encode(c.cmd));
    }
}
