/* 
 * File:   uart.cpp
 * Author: user
 * 
 * Created on 1 Февраль 2015 г., 16:13
 */

#include "uart.h"

// ----------------------------------------------------------------------------
CUart::CUart()
: fd(-1),
  pfd_notif(NULL),
  pfd_except_notif(NULL),
  updated_read(false)
{
}

// ----------------------------------------------------------------------------
CUart::~CUart() {
    Close();
}

// ----------------------------------------------------------------------------
bool CUart::Open(QString dev, const t_uart_prm &set)
{
    if(!open_fd(dev))
        return false;

    setting = set;
    if(!set_setting()) {
        close_fd();
        return false;
    }

    // создаем нотифер
    if(!create_notifier(&pfd_notif, QSocketNotifier::Read)) {
        restore_setting();  // восстанавливаем настройки
        close_fd();         // закрываем файл
        return false;
    }

    updated_read = false;   // сбрасываем флаг поступления новых данных
    connect(pfd_notif, SIGNAL(activated(int)), this, SLOT(slot_notifier_activated(int)));

    if(create_notifier(&pfd_except_notif, QSocketNotifier::Exception)) {
        connect(pfd_except_notif, SIGNAL(activated(int)),
                this, SLOT(slot_except_notifier_activated(int)));
    }

    return true;
}

// ----------------------------------------------------------------------------
void CUart::Close()
{
    if(fd != -1)
    {
        // удаляем нотифер
        delete_notifier(&pfd_notif);
        delete_notifier(&pfd_except_notif);

        restore_setting();

        close_fd();

        updated_read = false;   // сбрасываем флаг поступления новых данных
    }
}

// ----------------------------------------------------------------------------
bool CUart::create_notifier(QSocketNotifier **psn, QSocketNotifier::Type type) {

    bool rez = false;
    if(*psn) {
        delete *psn;
    }
    try {
        *psn = new QSocketNotifier(fd, type);
        rez = true;
    }
    catch (...) {
        trace(L_ERROR, "CUart::create_notifier(): Couldn't create socket notifier");
        *psn = NULL;
    }
    return rez;
}

// ----------------------------------------------------------------------------
void CUart::delete_notifier(QSocketNotifier **psn) {

    delete *psn;
    *psn = NULL;
}

// ----------------------------------------------------------------------------
ssize_t CUart::write(const QByteArray &ba)
{
    if(ba.size() > 0)
        return write(ba.constData(), (size_t)ba.size());
    else
        return ssize_t(0);
}

// ----------------------------------------------------------------------------
ssize_t CUart::write(const char *p_buf, size_t size)
{
    if(fd != -1)
    {
        return ::write(fd, p_buf, size);
    }
    else
    {
        // error: Файловый дескриптор не задан
        return ssize_t(0);
    }
}

// ----------------------------------------------------------------------------
ssize_t CUart::readAll(char *p_buf, size_t size)
{
    ssize_t rez;
    if(fd != -1)
    {
        updated_read = false;   // сбрасываем флаг поступления новых данных
        rez = ::read(fd, (void *)p_buf, size);
        if(pfd_notif) {
            pfd_notif->setEnabled(true);
        }
        return rez;
    }
    else
    {
        // error: Файловый дескриптор не задан
        return ssize_t(0);
    }
}

// ----------------------------------------------------------------------------
bool CUart::open_fd(const QString &device_name)
{
    // O_RDWR - открывает файл для чтения и для записи
    // O_NOCTTY - не открывает файл терминала как управляющий терминал
    // O_NDELAY - выполнять неблокирующие опреации с файлом (эквивалентно O_NONBLOCK в Linux)
    // O_NDELAY говоирт о том, что программа не заботиться о состоянии сигнала DCD (в RS-232),
    // иначе процесс "заснет" до тех пор пока на линии DCD не появится уровень space (off).
    fd = open(device_name.toLocal8Bit().constData(), O_RDWR | O_NOCTTY | O_NDELAY);
    if(fd == -1)
    {
        // error: Невозможно открыть файл
        trace(L_ERROR, "CUart: Couldn't open file");
        return false;
    }

    // FNDELAY - выполнять неблокирующее чтение из файла
    //fcntl(fd, F_SETFL, FNDELAY); // O_NDELAY уже задан
    return true;
}

// ----------------------------------------------------------------------------
bool CUart::close_fd()
{
    if(close(fd) == -1)
    {
        // error: Невозможно закрыть файл
        trace(L_ERROR, QString("CUart::close_fd(): Couldn't close %1").arg(fd).toLatin1());
        return false;
    }

    fd = -1;
    return true;
}

// ----------------------------------------------------------------------------
bool CUart::set_setting()
{
    // сохраняем текущие настройки порта
    tcgetattr(fd, &old_tio);

    // очищаем структуру под новые настройки
    bzero(&new_tio, sizeof(new_tio));

    // CLOCAL  : локальное соединение, без управления модемом
    // CREAD   : разрешаем получать символы
    new_tio.c_cflag |= setting.baudrate | setting.data | setting.parity | CLOCAL | CREAD;
    // flow control
    new_tio.c_cflag |= setting.flow;

    //
    if((setting.parity == EVEN) || (setting.parity == ODD))
        new_tio.c_iflag |= INPCK | ISTRIP;
    // игнорируем байты с ошибками четности
  //  new_tio.c_iflag |= IGNPAR;

    new_tio.c_oflag = 0;

    // устанавливаем режим ввода raw (как есть)
    new_tio.c_lflag = 0;

    // сбрасываем модемную линию и активируем настройки порта
    tcflush(fd, TCIFLUSH);
    if(tcsetattr(fd,TCSANOW,&new_tio) == -1)
    {
        // error
        return false;
    }

    return true;
}

// ----------------------------------------------------------------------------
bool CUart::restore_setting() const
{
    // восстанавливаем исходные настройки порта
    if(tcsetattr(fd,TCSANOW,&old_tio) == -1)
    {
        // error
        return false;
    }
    return true;
}

// ----------------------------------------------------------------------------
void CUart::slot_notifier_activated(int)
{
    updated_read = true;    // выставляем флаг постепления новых данных
    if(pfd_notif) {
        pfd_notif->setEnabled(false);
    }
    emit s_ready_read();
}

// ----------------------------------------------------------------------------
void CUart::slot_except_notifier_activated(int) {

//    trace(L_WARNING, "CUart::slot_except_notifier_activated()");
    if(pfd_except_notif) {
        pfd_except_notif->setEnabled(false);
    }

    Close();

    emit s_exception();
}
