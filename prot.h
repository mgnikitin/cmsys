#ifndef PROT_H
#define PROT_H

#include <QObject>
#include "queuebuf.h"
#include "cmd.h"

#define MAX_FIELD_COUNT 20

#define BUFFER_IN_SIZE  400

// максимальная длина поля hex
#define MAX_HEX_FIELD_SZ 16

class CProt : public QObject
{
    Q_OBJECT
public:

    struct t_field {
        uint8_t *ptr;
        size_t len;
    };

    struct t_hex {
        uint8_t arr[MAX_HEX_FIELD_SZ/2];
    };

    typedef bool (CProt::*pFunc)(const t_field *, size_t);

    /** \brief Структура, описывающая предложение
      */
    struct t_sent
    {
        QString header;
        pFunc func;
    };

    CProt();

    virtual ~CProt();

    static uint8_t CalcCS(const uint8_t *arr, size_t sz);

   static int make_CMSMD(uint8_t *arr, cmd_t cmd);

protected:

    t_field arr_fld[MAX_FIELD_COUNT];

    CQueueBuffer buf_all;       ///< \brief Входной массив

private:

    int sent_sz;
    t_sent sent[5];

    unsigned short remove_overflow(unsigned short bufferBytesCount, CQueueBuffer &buf);

    bool parse_hex(const uint8_t *arr, size_t sz, t_hex &val);

    bool parse_content(uint8_t *arr, size_t sz, int &fld_num);

    size_t field_bound(uint8_t *arr, size_t sz);

    bool parse_CNSMD(const CProt::t_field *arr_fld, size_t fld_cnt);

    bool parse_CMSMD(const CProt::t_field *arr_fld, size_t fld_cnt);

    bool parse_RTRMD(const CProt::t_field *arr, size_t fld_cnt);

    bool parse_RTRDT(const CProt::t_field *arr_fld, size_t fld_cnt);

    void parse_sentence(CQueueBuffer &buf);

    void loop_find(CQueueBuffer &qbuf);

public slots:

    void slot_parse(QByteArray);

signals:

    void s_new_cmd(cmd_t);
};

#endif // PROT_H
