#ifndef TGGATE_H
#define TGGATE_H

#include "gate.h"
#include <QNetworkAccessManager>
#include <QSslError>
#include <QNetworkReply>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QTime>
#include <QHttpPart>

Q_DECLARE_LOGGING_CATEGORY(tgGate)

class TgGate : public CCommGate
{
    Q_OBJECT
public:
    TgGate(uint8_t addr_own_, QString token_);
    ~TgGate();

    void sendCmd(cmd_t);

private:

    const uint8_t addr_own;

    struct Arg {
        QString name;
        QString value;
        QString mimeType;

        Arg(QString name_, QVariant value_, QString mimeType_ = "text/plain")
            : name(name_), value(value_.toString()), mimeType(mimeType_) {}


        static QHttpPart toHttpPart(QString name_, QVariant value_) {
            QHttpPart httpPart;
            httpPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant::fromValue(QString("form-data; name=\"%1\"").arg(name_)));
            httpPart.setBody(value_.toByteArray());
            return httpPart;
        }
    };

    struct User {
        qint32 id;
        QString first_name;
        QString last_name;
        QString username;

        User() : id(-1) {}
    };

    struct Chat {
        qint32 id;
        QString type;
        QString title;
        QString username;
        QString first_name;
        QString last_name;
        bool all_members_are_administrators;

        Chat() : id(-1) {}
    };

    struct PhotoSize {
        QString file_id;
        int width;
        int height;
        int file_size;      // optional
    };

    struct Message {
        qint32 message_id;
        User from;
        qint32 date;
        Chat chat;
        // ...
        QString text;                   // optional
        std::vector<PhotoSize> photo;   // optional

        Message() : message_id(-1) {}
    };

    struct Location {
        float longitude;
        float latitude;
    };

    struct InlineQuery {
        qint32 id;
        User from;
        Location location;
        QString query;
        QString offset;

        InlineQuery() : id(-1) {}
    };

    struct ChosenInlineResult {
        qint32 result_id;
        User from;
        Location location;
        QString inline_message_id;
        QString query;
    };

    struct CallbackQuery {
        qint32 id;
        User from;
        Message message;
        QString inline_message_id;
        QString chat_instance;
        QString data;
        QString game_short_name;
    };

    struct Update {
        qint32 update_id;
        Message message;
        Message edited_message;
        Message channel_post;
        Message edited_channel_post;
        InlineQuery inline_query;
        ChosenInlineResult chosen_inline_result;
        CallbackQuery callback_query;

        Update() : update_id(-1) { }
    };

    typedef std::vector<Update> UpdateArray;

    const QString m_token;
    qint32 m_lastUpdateId;
    QNetworkAccessManager qnam;
    QTime m_reqTime;
    bool m_updating;
    QHash <QUuid, Message> m_tempStorage;

    void getMe();
    void getUpdates(int32_t offset, int32_t limit = 100, int32_t timeout = 0, const QStringList &allowedUpdates = QStringList());
    void sendMessage(qint32 chartId_, QString text_, qint32 replyId_);
    void sendImage(qint32 chartId_, const QImage &img, QString caption_, int32_t replyId_, bool disableNotification_ = true);

    QString getUrl(QString method) const;
    QNetworkReply *generateRequest(QString method, const std::vector<Arg>& args);

    template <typename T> void startRequest(QString method, const std::vector<Arg> &args = std::vector<Arg>()) {
        m_reqTime.restart();
        QNetworkReply *reply = generateRequest(method, args);
        connect(reply, &QNetworkReply::finished, [this, reply]() { finished<T>(reply); } );
        connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
                this, &TgGate::slotError);
        connect(reply, &QNetworkReply::sslErrors, this, &TgGate::slotSslErrors);
    }

    template <typename T> void startRequestMulti(QString method, QHttpMultiPart *multiPart_) {
        m_reqTime.restart();
        QNetworkRequest request;
        request.setUrl(getUrl(method));
        QNetworkReply *reply = qnam.post(request, multiPart_);
        multiPart_->setParent(reply);
        connect(reply, &QNetworkReply::finished, [this, reply]() { finished<T>(reply); } );
        connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
                this, &TgGate::slotError);
        connect(reply, &QNetworkReply::sslErrors, this, &TgGate::slotSslErrors);
    }

    QString generateWwwFormUrlencoded(const std::vector<Arg>& args);

    QString generateRandomString(size_t length);

    TYPE_CMD parseCommand(const Message &mes);

    template <typename T> T convJsonValue(const QJsonValue &, bool &ok);

    template<typename T> void finished(QNetworkReply *reply) {
        QJsonParseError error;
        bool ok = true;
        QJsonObject jobj;
        Q_ASSERT(reply);
        QByteArray ba = reply->readAll();
        reply->deleteLater();
        QJsonDocument jdoc = QJsonDocument::fromJson(ba, &error);
        qCDebug(tgGate) << QString("received %1").arg(QString(ba));
        if(error.error != QJsonParseError::NoError) {
            qCWarning(tgGate) << QString("json error: %1").arg(error.errorString());
            ok = false;
            handleObject<T>(T(), false);
        }
        else {
            if(ok) { jobj = jdoc.object(); }
            bool rez = convJsonValue<bool>(jobj.value("ok"), ok);
            if(!rez) { qCWarning(tgGate) << QString("not ok"); ok = false; }
            if(ok) {
                const T &obj = convJsonValue<T>(jobj.value("result"), ok);
                handleObject<T>(obj, ok);
            }
            else {
                handleObject<T>(T(), false);
            }
        }
    }

    void waitUpdate();

    template <typename T> void handleObject(const T &, bool);

private slots:

    void slotError(QNetworkReply::NetworkError);
    void slotSslErrors(const QList<QSslError> &lst);
};

#endif // TGGATE_H
