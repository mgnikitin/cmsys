#ifndef UDPSOCKETGATE_H
#define UDPSOCKETGATE_H

#include "gate.h"
#include <QUdpSocket>
#include <QMap>
#include "prot.h"

class CUdpSocketGate : public CCommGate
{
    Q_OBJECT
public:
    CUdpSocketGate(quint16 port);

    virtual ~CUdpSocketGate();

    bool Open();

    void sendCmd(cmd_t);

    void addHost(uint8_t addr, QHostAddress host, quint16 port);

private:

    QUdpSocket m_socket;
    CProt prot;

    quint16 listen_port;

    struct host_t {
        QHostAddress host;
        quint16 port;
    };

    QMap<uint8_t, host_t> map_host;

    host_t last_host;

    bool send_data(const uint8_t *arr, size_t len, host_t host);

private slots:

    void ev_open();

    void slot_read_datagram();

    void slot_new_cmd(cmd_t cmd);

signals:

    void s_parse(QByteArray);
};

#endif // UDPSOCKETGATE_H
