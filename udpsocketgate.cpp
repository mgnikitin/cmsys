#include "udpsocketgate.h"

// ----------------------------------------------------------------------------
CUdpSocketGate::CUdpSocketGate(quint16 port)
    : listen_port(port)
{
    connect(&prot, &CProt::s_new_cmd, this, &CUdpSocketGate::slot_new_cmd);
    connect(this, SIGNAL(s_parse(QByteArray)), &prot, SLOT(slot_parse(QByteArray)));

    QTimer::singleShot(0, this, SLOT(ev_open()));
}

// ----------------------------------------------------------------------------
CUdpSocketGate::~CUdpSocketGate() {

}

// ----------------------------------------------------------------------------
bool CUdpSocketGate::Open() {

    if(m_socket.bind(QHostAddress::Any, listen_port)) {
        trace(L_NOTIFY, QObject::trUtf8("udp-server started on port %1").arg(listen_port).toLatin1());
        connect(&m_socket, SIGNAL(readyRead()), this, SLOT(slot_read_datagram()));
    }
    else {
        trace(L_WARNING, QObject::trUtf8("Couldn't start udp-server %1").arg(m_socket.errorString()).toLatin1());
        return false;
    }
    return true;
}

// ----------------------------------------------------------------------------
void CUdpSocketGate::sendCmd(cmd_t cmd) {

    uint8_t arr_out[BUFFER_IN_SIZE];
    if(!map_host.contains(cmd.addr_trg)) {
        trace(L_WARNING, QString("CUdpSocketGate::sendCmd(): Couldn't find host for address 0x%1")
              .arg(cmd.addr_trg, 2, 16, QChar('0')).toLatin1());
        return;
    }
    int len = prot.make_CMSMD(arr_out, cmd);
    send_data(arr_out, len, map_host.value(cmd.addr_trg));
}

// ----------------------------------------------------------------------------
void CUdpSocketGate::addHost(uint8_t addr, QHostAddress host, quint16 port) {

    host_t h;
    h.host = host;
    h.port = port;
    map_host.insert(addr, h);
}

// ----------------------------------------------------------------------------
bool CUdpSocketGate::send_data(const uint8_t *arr, size_t len, host_t host) {

    ssize_t sz = m_socket.writeDatagram((const char *)arr, len, host.host, host.port);
    if(sz == -1) {
        trace(L_ERROR, "CUdpSocketGate::send_data(): Couldn't write to socket:");
        return false;
    }
    trace(L_TRACE, "CUdpSocketGate: Send data to socket");
    return (sz == (ssize_t)len);
}

// ----------------------------------------------------------------------------
void CUdpSocketGate::ev_open() {

    if(!Open()) {
        trace(L_ERROR, QObject::trUtf8("CUdpSocketGate::Open(): Couldn't bind to host").toLatin1());
        CEventMngr::instance().addEvent(this, &CUdpSocketGate::ev_open, 3600);
    }
}

// ----------------------------------------------------------------------------
void CUdpSocketGate::slot_read_datagram() {

    while (m_socket.hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(m_socket.pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        //trace(L_TRACE, "CUdpSocketGate: read datagram");
        m_socket.readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        last_host.host = sender;
        last_host.port = senderPort;

        if(datagram.size() > 0)
        {
            emit s_parse(datagram);
        }
    }
}

// ----------------------------------------------------------------------------
void CUdpSocketGate::slot_new_cmd(cmd_t cmd) {

    map_host.insert(cmd.addr_src, last_host);
//    trace(L_TRACE, "CUdpSocketGate: received new command");
    emit s_received_cmd(cmd);
}
