#include <QtCore/qdatetime.h>

#include "db_inter.h"
#include "trace.h"

#define DB_DRIVER_TYPE "QMYSQL"

const QString dt_fmt = "yyyy-MM-dd hh:mm:ss";

// -----------------------------------------------------------------------------
DBInter::DBInter(uint8_t addr, db_prm_t cfg)
    : addr_own(addr),
      id(QUuid::createUuid()),
      db_cfg(cfg)
{
    f_stop = true;
    dt_fr = QDateTime::currentDateTime();
    trace(L_NOTIFY, LF_DB, QObject::trUtf8("Query from %1").arg(dt_fr.toString(dt_fmt)).toLatin1());
}

// -----------------------------------------------------------------------------
DBInter::~DBInter()
{
    slot_disconnect();
}

// -----------------------------------------------------------------------------
void DBInter::slot_connect()
{
    if(!db.connectionNames().contains(DB_DRIVER_TYPE))
        db = QSqlDatabase::addDatabase(DB_DRIVER_TYPE);
//    trace(L_DEBUG, LF_DB, QSqlDatabase::drivers().toAscii());
    if(db.isOpen())
        db.close();

    db.setHostName(db_cfg.db_host);
    db.setDatabaseName(db_cfg.db_name);
    db.setUserName(db_cfg.db_user);
    db.setPassword(db_cfg.db_password);

    bool ok = db.open();
    if(!ok)
    {
        trace(L_WARNING, LF_DB, QObject::trUtf8("DBInter::slot_connect(): Couldn't open db").toLatin1());
        QSqlError sql_err = db.lastError();
        if(sql_err.isValid())
            trace(L_WARNING, LF_DB, QObject::trUtf8("SQLError: %1").arg(sql_err.text()).toLatin1());
        return;
    }
    else
        trace(L_NOTIFY, LF_DB, QObject::trUtf8("DBInter: connection for %1 ok").arg(db.userName()).toLatin1());

    f_stop = false;
//    db_query_field_names();
    slot_db_query();
}

// -----------------------------------------------------------------------------
void DBInter::slot_disconnect()
{
    f_stop = true;
    if(db.isOpen())
        db.close();
}

// -----------------------------------------------------------------------------
void DBInter::slot_db_query()
{
    if(!f_stop)
    {
        db_query();
        QTimer::singleShot(1000, this, SLOT(slot_db_query()));
    }
}

// -----------------------------------------------------------------------------
void DBInter::db_query_field_names()
{
    if(db.isOpen())
    {
        QSqlQuery query("SELECT * FROM inbox;", db);
        query.first();
        for (int i = 0; i < query.record().count(); i++)
        {
            trace(L_DEBUG, LF_DB, query.record().fieldName(i).toLatin1());
        }
    }
    else
        trace(L_WARNING, LF_DB, QObject::trUtf8("DBInter::db_query_field_names(): db is not open.").toLatin1());

}

// -----------------------------------------------------------------------------
void DBInter::db_query() {

    if(db.isOpen()) {
        QString query_str = QObject::trUtf8("SELECT ReceivingDateTime, SenderNumber, TextDecoded FROM inbox WHERE ReceivingDateTime > '%1';")
                .arg(dt_fr.toString(dt_fmt));
        //trace(L_DEBUG, LF_DB, query_str.toAscii());
        QSqlQuery query(query_str, db);

        while(query.next()) {
            QSqlRecord rec = query.record();
    //        trace(L_DEBUG, LF_DB, rec.value(0).toString() << rec.value(1).toString().toAscii());
            dt_fr = rec.value(0).toDateTime(); // обновляем запрашиваемое время из БД
            parse_cmd(rec.value(1).toString(), rec.value(2).toString());
        }
    }
    else {
        trace(L_WARNING, LF_DB, QObject::trUtf8("DBInter::db_query(): db is not open.").toLatin1());
    }
}

// -----------------------------------------------------------------------------
QString DBInter::cmd_to_query(cmd_t cmd) {

    QString query_str;
    QString number;

    if(!map_number.contains(cmd.resp_cmd)) {
        return QString();
    }

    const QString response = StringCmdConv::encode(cmd.cmd);

    number = map_number.value(cmd.resp_cmd);
    query_str = QObject::trUtf8("INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID,   Coding) \
             VALUES ('%1', '%2',  'CMSys', 'Default_No_Compression');").arg(number).arg(response);

    map_number.remove(cmd.resp_cmd);
    return query_str;
}

// -----------------------------------------------------------------------------
void DBInter::parse_cmd(QString number, QString str)
{
    cmd_t cmd_prm = CommandGenerator::generate(0x00, addr_own, CMD_NONE);  // TODO
    
    str = str.trimmed().toLower();
    cmd_prm.cmd = StringCmdConv::decode(str);
    map_number.insert(cmd_prm.cmd, number);
    emit s_req_cmd(id, cmd_prm);
}

// -----------------------------------------------------------------------------
void DBInter::slot_response_cmd(QUuid uid, cmd_t cmd) {
    
    if(uid != id)
        return;

    QString query_str = cmd_to_query(cmd);
    trace(L_DEBUG, LF_DB, query_str.toLatin1());
    QSqlQuery query(db);
    if(!query.exec(query_str))
    {
        QSqlError sql_err = query.lastError();
        trace(L_WARNING, LF_DB,  QObject::trUtf8("error query exec: %1").arg(sql_err.text()).toLatin1());
    }
}
