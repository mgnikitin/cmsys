/* 
 * File:   eventmngr.h
 * Author: user
 *
 * Created on 7 Февраль 2015 г., 16:21
 */

#ifndef EVENTMNGR_H
#define	EVENTMNGR_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include <QVector>
#include <QList>
#include <QHash>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(eventHandler)

class CEventHandlerBase {
public:

    virtual void execute() = 0;
};

/** \brief Обработчик события
  */
template <typename T>
class CEventHandler : public CEventHandlerBase {
public:
    typedef void (T::*ptr_func)(void);

    CEventHandler(T *object, ptr_func function) : obj(object), func(function) {}

    void execute() {
        qCDebug(eventHandler) << QString("execute");
        (obj->*func)();
    }

private:

    T *obj;
    ptr_func func;

    CEventHandler(const CEventHandler &) = delete;
    CEventHandler &operator ==(const CEventHandler &ev)  = delete;

};

Q_DECLARE_LOGGING_CATEGORY(eventMngr)
/** \brief Менеджер событий
  */
class CEventMngr : public QObject {
    Q_OBJECT
public:

    struct t_event
    {
        bool updated;
        int interval;
        CEventHandlerBase *ev_handlr;
    };

    virtual ~CEventMngr();
    
    void startEvents();

    template <typename T>
    bool addEvent(T *obj, void (T::*func) (void), const int &interval_sec) {
        qCDebug(eventMngr) << QString("addEvent");
        t_event item;
        item.updated = (bypass) ? true : false;
        item.interval = interval_sec;
        item.ev_handlr = new CEventHandler<T>(obj, func);
        insert(item);

        return true;
    }

    static CEventMngr &instance() {
        static CEventMngr instance_;
        return instance_;
    }
    
private:

    QList<CEventMngr::t_event> events;
    QTimer *timer;
    bool bypass;

    CEventMngr();
    
    void insert(const CEventMngr::t_event &ev);
    
    bool remove(CEventHandlerBase *evh);
    
//    bool find(ptr_func event_func);
    
private slots:    
    void bypass_events();
};

#endif	/* EVENTMNGR_H */

