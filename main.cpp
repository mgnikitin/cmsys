/* 
 * File:   main.cpp
 * Author: Asus
 *
 * Created on 29 Сентябрь 2014 г., 21:35
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <signal.h>
#include <sys/file.h>
#include <sys/ucontext.h>
#include <errno.h>

#include "daemonapp.h"
#include "trace.h"

static const char *DAEMON_IDENT = "cmsysd";

static int fd_pid = -1;
static bool use_daemon = false;

// обработчик сигналов исключений
static void signal_error(int sig, siginfo_t *si, void */*ptr*/)
{
    trace(L_ERROR, "signal_error(): %s, Addr: 0x%0.16X", strsignal(sig), si->si_addr);

    _exit(EXIT_FAILURE);
}

// создание блокировочного файла процесса
int createPidFile(const char *pid_file)
{
    int fd;
    char buf[255];

    fd = open(pid_file, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if(fd == -1)
    {
        trace(L_ERROR, "Can't open pid file '%s': %s", pid_file, strerror(errno));
        return -1;
    }

    // попытка забликоровать файл
    if(flock(fd, LOCK_EX | LOCK_NB) == -1)
    {
        if(EWOULDBLOCK == errno)
            trace(L_ERROR, "pid file '%s' already locked by another instanse of daemon", pid_file);
        else
            trace(L_ERROR, "flock error pid file '%s'", pid_file);
        return -1;
    }

    // стираем предыдущую запись
    if(ftruncate(fd, 0) == -1)
    {
        trace(L_ERROR, "Can't truncate pid file '%s'", pid_file);
        return -1;
    }

    // записываем информацию об идентификаторе процесса в файл
    snprintf(buf, (size_t)255, "%ld\n", (long)getpid());
    if(write(fd, buf, strlen(buf)) != (ssize_t)strlen(buf))
    {
        trace(L_ERROR, "Error with writting to pid file '%s'", pid_file);
        return -1;
    }

    return fd;
}

// установить приложение как демон
int become_daemon()
{
    int fd;
//    int _maxfd; // наибольший файловый дескриптор
    struct sigaction sa_hup;
    struct sigaction sa_term;
    struct sigaction sa_err;
    
    //openlog(log_file, LOG_PID | LOG_CONS, LOG_LOCAL0);
    
    // инициализируем обработчик сигнала
    sigemptyset(&sa_hup.sa_mask);
    sa_hup.sa_flags = SA_RESTART;
    sa_hup.sa_handler = CDaemonApp::sighup_handler;
    if(sigaction(SIGHUP, &sa_hup, NULL) == -1)
    {
        trace(L_ERROR, "become_daemon(): Couldn't set signal: %s(%d)", strerror(errno), errno);
        return -1;
    }

    sigemptyset(&sa_term.sa_mask);
    sa_term.sa_handler = CDaemonApp::sigterm_handler;
    if(sigaction(SIGTERM, &sa_term, NULL) == -1)
    {
        trace(L_ERROR, "become_daemon(): Couldn't set signal: %s(%d)", strerror(errno), errno);
        return -1;
    }
    
    sigemptyset(&sa_err.sa_mask);
    sa_err.sa_flags = SA_SIGINFO;
    sa_err.sa_sigaction = signal_error;
    sigaction(SIGFPE, &sa_err, 0); // ошибка FPU
    sigaction(SIGILL, &sa_err, 0); // ошибочная инструкция
    sigaction(SIGSEGV, &sa_err, 0); // ошибка доступа к памяти
    sigaction(SIGBUS, &sa_err, 0); // ошибка шины, при обращении к физической памяти
    
    // создаем фоновый процесс
    switch(fork())
    {
    case -1:
        trace(L_ERROR, "become_daemon(): Couldn't fork: %s(%d)", strerror(errno), errno);
        return -1;
    case 0:  break;                 // дочерний процесс создан
    default: exit(EXIT_SUCCESS);    // закрываем родительский процесс
    }
    
    // создаем новую сессию
    if(setsid() == -1)              // являемся лидером сессии
    {
        trace(L_ERROR, "become_daemon(): Couldn't create new session: %s(%d)", strerror(errno), errno);
        return -1;                 
    }
    
    trace(L_NOTIFY, "Starting daemon");
 
    // очищаем маску режима создания файлов
    umask(0);
    
    // меняем директорию на корневую
    chdir("/");
    
    // закрытие файловых дескрипторов
 //   _maxfd = sysconf(_SC_OPEN_MAX);
 //   if(_maxfd == -1)
 //       _maxfd = 8192;
 //   for(fd = 0; fd < _maxfd; fd++)
 //       close(fd);
    
    //
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    fd = open("/dev/null", O_RDWR);
    if(fd != STDIN_FILENO) {
        trace(L_ERROR, "fd isn't STDIN_FILENO\n");
        return -1;
    }
    if(dup2(STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO) {
        trace(L_ERROR, "dup isn't STDOUT_FILENO\n");
        return -1;
    }
    if(dup2(STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO) {
        trace(L_ERROR, "dup isn't STDERR_FILENO\n");
        return -1;
    }
    
    return 0;
}

void parse_args(int argc, char** argv) {
    
    for(int i = 1; i < argc; i++) {
        if(strcmp("--daemon", argv[i]) == 0)
            use_daemon = true;
        else
            trace(L_WARNING, "undefined param %s", argv[i]);
    }
}

/*
 * 
 */
int main(int argc, char** argv) 
{
    char buf[255];
    gid_t gid;
    struct passwd *pw;
    

    if(use_daemon) {       
        gid = getgid();

        // проверка запуска под root
        pw = getpwnam("root");
        if(pw->pw_gid != gid) {
            printf("Need run under root\n");
            return 0;
        }
    } 
    
    setup_trace(/*argv[0]*/ DAEMON_IDENT, L_TRACE, LF_ALL);
    
    parse_args(argc, argv);    

    if(use_daemon) {
        // установить приложение как демон
        if(become_daemon() == -1) {
            trace(L_ERROR, "become_daemon() error");
            return -1;
        }

        // проверка повторного запуска демона
        sprintf(buf, "/var/run/%s.pid", DAEMON_IDENT);   // TODO: проверить результат
        fd_pid  =createPidFile(buf);
        if(fd_pid == -1) {
            trace(L_ERROR, "createPidFile() error");
            return -1;
        }
    }
    CDaemonApp daemon(argc, argv);
    return daemon.exec();
}

