/* 
 * File:   trace.h
 * Author: user
 *
 * Created on 29 Октябрь 2014 г., 18:37
 */

#ifndef TRACE_H
#define	TRACE_H

enum LOG_LEVEL
{
    L_DEBUG_ERROR,
    L_ERROR,
    L_WARNING,
    L_DEBUG,
    L_NOTIFY,
    L_TRACE
};

enum LOG_FACILITY {
    LF_DB       = 0x0001,
    LF_ALL      = 0xFFFF
};

void setup_trace(const char * ident, LOG_LEVEL lev, LOG_FACILITY facility_map);

void trace(LOG_LEVEL lev, const char *fmt, ...);

void trace(LOG_LEVEL lev, LOG_FACILITY facility, const char *fmt, ...);

#endif	/* TRACE_H */

