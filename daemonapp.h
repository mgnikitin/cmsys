/* 
 * File:   daemonapp.h
 * Author: user
 *
 * Created on 1 Ноябрь 2014 г., 11:48
 */
#include <QtCore/QCoreApplication>
#include <QSocketNotifier>
#include "cmsmngr.h"

#ifndef DAEMONAPP_H
#define	DAEMONAPP_H

class CDaemonApp : public QCoreApplication 
{
    Q_OBJECT
public:
    CDaemonApp(int & argc, char ** argv);
    virtual ~CDaemonApp();
    
    // handling unix signals
    static void sighup_handler(int);
    static void sigterm_handler(int);
    
private:

    CMSMngr *cms_mngr;
    
    static int fd_sighup[2];
    static int fd_sigterm[2];
    
    QSocketNotifier *sighup_notifier;
    QSocketNotifier *sigterm_notifier;
    
private slots:
        
    void slot_sighup(int);
    void slot_sigterm(int);
};

#endif	/* DAEMONAPP_H */

