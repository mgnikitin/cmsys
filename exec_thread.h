#ifndef DB_THREAD_H
#define DB_THREAD_H

#include <QThread>

class CExecThread : public QThread
{
    Q_OBJECT
public:
    CExecThread();
    virtual ~CExecThread();

protected:

    void run();
};

#endif // DB_THREAD_H
