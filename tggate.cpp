#include "tggate.h"
#include <QImage>
#include <QMimeData>
#include <QBuffer>
#include <QHttpMultiPart>

Q_LOGGING_CATEGORY(tgGate, "tgGate")

// ----------------------------------------------------------------------------
TgGate::TgGate(uint8_t addr_own_, QString token_)
    : addr_own(addr_own_), m_token(token_)
{
    m_lastUpdateId = 0;
    m_updating = false;
    getMe();
}

// ----------------------------------------------------------------------------
TgGate::~TgGate() {

}

// ----------------------------------------------------------------------------
void TgGate::sendCmd(cmd_t c) {

    if(!m_tempStorage.contains(c.uid)) {
        qCWarning(tgGate) << QString("Couldn't find data");
        return;
    }
    Message mes = m_tempStorage.value(c.uid);
    m_tempStorage.remove(c.uid);
    if((CMD_ACCPT == c.cmd) && (CMD_IMAGE == c.resp_cmd)) {
        sendImage(mes.chat.id, qvariant_cast<QImage>(c.data), "photo", 0);
    }
    else {
        sendMessage(mes.chat.id, StringCmdConv::reply(c), 0);
    }
}

// ----------------------------------------------------------------------------
void TgGate::getMe() {

    startRequest<User>("getMe");
}

// ----------------------------------------------------------------------------
void TgGate::getUpdates(int32_t offset, int32_t limit, int32_t timeout, const QStringList &) {

    m_updating = false;
    std::vector<Arg> args;
    if (offset) {
        args.emplace_back(Arg("offset", QString::number(offset)));
    }
    limit = qMax(1, qMin(100, limit));
    args.emplace_back(Arg("limit", QString::number(limit)));
    if (timeout) {
        args.emplace_back(Arg("timeout", QString::number(timeout)));
    }

    startRequest<UpdateArray>("getUpdates", args);
}

// ----------------------------------------------------------------------------
void TgGate::sendMessage(qint32 chartId_, QString text_, qint32 replyId_) {

    std::vector<Arg> args;
    args.emplace_back(Arg("chat_id", QString::number(chartId_)));
    args.emplace_back(Arg("text", text_));
//    args.emplace_back(Arg("parse_mode", ));
    args.emplace_back(Arg("disable_web_page_preview", true));
    args.emplace_back(Arg("disable_notification", false));
    args.emplace_back(Arg("reply_to_message_id", QString::number(replyId_)));
//    args.emplace_back(Arg("reply_markup", ));

    qCDebug(tgGate) << QString("text %1").arg(text_);

    startRequest<Message>("sendMessage", args);
}

// ----------------------------------------------------------------------------
void TgGate::sendImage(qint32 chartId_, const QImage &img, QString caption_, int32_t replyId_,
                       bool disableNotification_) {

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    // ----
    multiPart->append(Arg::toHttpPart("chat_id", QString::number(chartId_)));
    // ----
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    img.save(&buffer, "JPEG");
    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"photo\"; filename=\"file.jpeg\""));
    imagePart.setBody(ba);
    multiPart->append(imagePart);
    // ---
    if (!caption_.isEmpty()) {
        multiPart->append(Arg::toHttpPart("caption", caption_));
    }
    if (replyId_) {
        multiPart->append(Arg::toHttpPart("reply_to_message_id", replyId_));
    }
    if (disableNotification_){
        multiPart->append(Arg::toHttpPart("disable_notification", disableNotification_));
    }

    startRequestMulti<Message>("sendPhoto", multiPart);
}

// ----------------------------------------------------------------------------
QString TgGate::getUrl(QString method) const {

    QString url = "https://api.telegram.org/bot";
    url += m_token;
    url += "/";
    url += method;
    return url;
}

// ----------------------------------------------------------------------------
QNetworkReply *TgGate::generateRequest(QString method, const std::vector<Arg>& args) {

    bool isKeepAlive = false;
    QNetworkReply *reply;

    QNetworkRequest request;
    QString requestData;
    request.setUrl(getUrl(method));
    if (!args.empty()) {
        request.setHeader(QNetworkRequest::ContentTypeHeader,QString::fromUtf8("application/x-www-form-urlencoded"));
        requestData = generateWwwFormUrlencoded(args);
        if (isKeepAlive) {
            request.setRawHeader("Connection","keep-alive");
        } else {
            request.setRawHeader("Connection","close");
        }
        request.setHeader(QNetworkRequest::ContentLengthHeader, QVariant(requestData.length()));
    }

    if(args.empty()) {
        reply = qnam.get(request);
    }
    else {
        reply = qnam.post(request, requestData.toUtf8());
    }
    return reply;
}

// ----------------------------------------------------------------------------
template <>
double TgGate::convJsonValue<double>(const QJsonValue &val, bool &ok) {

    if(!val.isDouble()) {
        qCWarning(tgGate) << QString("not double %1").arg(val.type());
        ok = false;
        return -1;
    }

    ok = true;
    return val.toDouble(0);
}

// ----------------------------------------------------------------------------
template <>
int TgGate::convJsonValue<int>(const QJsonValue &val, bool &ok) {

    return convJsonValue<double>(val, ok);
}

// ----------------------------------------------------------------------------
template <>
bool TgGate::convJsonValue<bool>(const QJsonValue &val, bool &ok) {

    if(!val.isBool()) {
        ok = false;
        return false;
    }

    ok = true;
    return val.toBool(false);
}

// ----------------------------------------------------------------------------
template <>
QString TgGate::convJsonValue<QString>(const QJsonValue &val, bool &ok) {

    if(!val.isString()) {
        ok = false;
        return QString();
    }

    ok = true;
    return val.toString();
}

// ----------------------------------------------------------------------------
template <>
TgGate::User TgGate::convJsonValue<TgGate::User>(const QJsonValue &jval, bool &ok) {

    User user;
    bool ok1;
    QJsonObject jobj = jval.toObject();
    user.id = convJsonValue<int>(jobj.value("id"), ok);
    if(!ok) { return user; }
    user.first_name = convJsonValue<QString>(jobj.value("first_name"), ok);
    if(!ok) { return user; }
    user.last_name = convJsonValue<QString>(jobj.value("last_name"), ok1);   // optional
    user.username = convJsonValue<QString>(jobj.value("username"), ok1);     // optional

    return user;
}

// ----------------------------------------------------------------------------
template <>
TgGate::Chat TgGate::convJsonValue<TgGate::Chat>(const QJsonValue &jval, bool &ok) {

    Chat chat;
    bool ok1;
    if(jval.isUndefined()) {
        ok = false;
        return chat;
    }

    if(!jval.isObject()) {
        qCWarning(tgGate) << QString("not object");
        ok = false;
        return chat;
    }

    QJsonObject jobj = jval.toObject();
    chat.id = convJsonValue<int>(jobj.value("id"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error id"); return chat; }
    chat.type = convJsonValue<QString>(jobj.value("type"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error type"); return chat; }
    chat.title = convJsonValue<QString>(jobj.value("title"), ok1);           // optional
    chat.username = convJsonValue<QString>(jobj.value("username"), ok1);     // optional
    chat.first_name = convJsonValue<QString>(jobj.value("first_name"), ok1); // optional
    chat.last_name = convJsonValue<QString>(jobj.value("last_name"), ok1);   // optional
    chat.all_members_are_administrators = convJsonValue<bool>(jobj.value("all_members_are_administrators"), ok1);   // optional

    return chat;
}

// ----------------------------------------------------------------------------
template <>
TgGate::Message TgGate::convJsonValue<TgGate::Message>(const QJsonValue &jval, bool &ok) {

    Message mes;
    if(jval.isUndefined()) {
        ok = false;
        return mes;
    }

    if(!jval.isObject()) {
        qCWarning(tgGate) << QString("not object");
        ok = false;
        return mes;
    }
    QJsonObject jobj = jval.toObject();
    mes.message_id = convJsonValue<int>(jobj.value("message_id"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error message_id"); return mes; }
    mes.from = convJsonValue<TgGate::User>(jobj.value("from"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error from"); return mes; }
    mes.date = convJsonValue<int>(jobj.value("date"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error date"); return mes; }
    mes.chat = convJsonValue<TgGate::Chat>(jobj.value("chat"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error chat"); return mes; }
    mes.text = convJsonValue<QString>(jobj.value("text"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error text"); return mes; }

    return mes;
}

// ----------------------------------------------------------------------------
template <>
TgGate::Location TgGate::convJsonValue<TgGate::Location>(const QJsonValue &jval, bool &ok) {

    Location loc;
    if(jval.isUndefined()) {
        ok = false;
        return loc;
    }
    QJsonObject jobj = jval.toObject();
    loc.longitude = convJsonValue<double>(jobj.value("longitude"), ok);
    if(!ok) { return loc; }
    loc.latitude = convJsonValue<double>(jobj.value("latitude"), ok);

    return loc;
}

// ----------------------------------------------------------------------------
template <>
TgGate::InlineQuery TgGate::convJsonValue<TgGate::InlineQuery>(const QJsonValue &jval, bool &ok) {

    InlineQuery iq;
    if(jval.isUndefined()) {
        ok = false;
        return iq;
    }
    QJsonObject jobj = jval.toObject();
    iq.id = convJsonValue<int>(jobj.value("id"), ok);
    if(!ok) { return iq; }
    iq.from = convJsonValue<TgGate::User>(jobj.value("from"), ok);
    if(!ok) { return iq; }
    iq.location = convJsonValue<TgGate::Location>(jobj.value("location"), ok);
    if(!ok) { return iq; }
    iq.query = convJsonValue<QString>(jobj.value("query"), ok);
    if(!ok) { return iq; }
    iq.offset = convJsonValue<QString>(jobj.value("offset"), ok);
    return iq;
}

// ----------------------------------------------------------------------------
template <>
TgGate::ChosenInlineResult TgGate::convJsonValue<TgGate::ChosenInlineResult>(const QJsonValue &jval, bool &ok) {

    ChosenInlineResult ciq;
    if(jval.isUndefined()) {
        ok = false;
        return ciq;
    }
    QJsonObject jobj = jval.toObject();
    ciq.result_id = convJsonValue<int>(jobj.value("result_id"), ok);
    if(!ok) { return ciq; }
    ciq.from = convJsonValue<TgGate::User>(jobj.value("from"), ok);
    if(!ok) { return ciq; }
    ciq.location = convJsonValue<TgGate::Location>(jobj.value("location"), ok);
    if(!ok) { return ciq; }
    ciq.inline_message_id = convJsonValue<QString>(jobj.value("inline_message_id"), ok);
    if(!ok) { return ciq; }
    ciq.query = convJsonValue<QString>(jobj.value("query"), ok);
    return ciq;
}

// ----------------------------------------------------------------------------
template <>
TgGate::CallbackQuery TgGate::convJsonValue<TgGate::CallbackQuery>(const QJsonValue &jval, bool &ok) {

    CallbackQuery cb;
    if(jval.isUndefined()) {
        ok = false;
        return cb;
    }
    QJsonObject jobj = jval.toObject();
    cb.id = convJsonValue<int>(jobj.value("id"), ok);
    if(!ok) { return cb; }
    cb.from = convJsonValue<TgGate::User>(jobj.value("from"), ok);
    if(!ok) { return cb; }
    cb.message = convJsonValue<TgGate::Message>(jobj.value("message").toObject(), ok);
    if(!ok) { return cb; }
    cb.inline_message_id = convJsonValue<QString>(jobj.value("inline_message_id"), ok);
    if(!ok) { return cb; }
    cb.chat_instance = convJsonValue<QString>(jobj.value("chat_instance"), ok);
    if(!ok) { return cb; }
    cb.data = convJsonValue<QString>(jobj.value("data"), ok);
    if(!ok) { return cb; }
    cb.game_short_name = convJsonValue<QString>(jobj.value("game_short_name"), ok);
    return cb;
}

// ----------------------------------------------------------------------------
template <>
TgGate::Update TgGate::convJsonValue<TgGate::Update>(const QJsonValue &jval, bool &ok) {

    Update upd;
    bool ok1;
    QJsonObject jobj = jval.toObject();
    upd.update_id = convJsonValue<int>(jobj.value("update_id"), ok);
    if(!ok) { qCWarning(tgGate) << QString("error update_id"); return upd; }
    upd.message = convJsonValue<TgGate::Message>(jobj.value("message"), ok1);   // optional
    upd.edited_message = convJsonValue<TgGate::Message>(jobj.value("edited_message"), ok1); // optional
    upd.channel_post = convJsonValue<TgGate::Message>(jobj.value("channel_post"), ok1);     // optional
    upd.edited_channel_post = convJsonValue<TgGate::Message>(jobj.value("edited_channel_post"), ok1);   // optional
    upd.inline_query = convJsonValue<TgGate::InlineQuery>(jobj.value("inline_query"), ok1); // optional
    upd.chosen_inline_result = convJsonValue<TgGate::ChosenInlineResult>(jobj.value("chosen_inline_result"), ok1); // optional
    upd.callback_query = convJsonValue<TgGate::CallbackQuery>(jobj.value("callback_query"), ok1); // optional

    return upd;
}

// ----------------------------------------------------------------------------
template <>
TgGate::UpdateArray TgGate::convJsonValue<TgGate::UpdateArray>(const QJsonValue &jval, bool &ok) {

    TgGate::UpdateArray uar;
    if(!jval.isArray()) {
        ok = false;
        qCWarning(tgGate) << QString("not array");
        return uar;
    }
    QJsonArray jarr = jval.toArray();
    for(int i = 0; i < jarr.count(); i++) {
        TgGate::Update upd = convJsonValue<TgGate::Update>(jarr.at(i), ok);
        if(ok) {
            uar.emplace_back(upd);
        }
    }

    return uar;
}

// ----------------------------------------------------------------------------
template <>
void TgGate::handleObject<TgGate::User>(const TgGate::User &user, bool ok) {

    if(ok) {
        qCDebug(tgGate) << QString("user %1 %2 %3 %4").arg(user.id).arg(user.first_name).arg(user.last_name).arg(user.username);
    }

    waitUpdate();
}

// ----------------------------------------------------------------------------
template <>
void TgGate::handleObject<TgGate::UpdateArray>(const TgGate::UpdateArray &uar, bool ok) {

    if(ok) {
        for(const TgGate::Update &upd : uar) {
            qCDebug(tgGate) << QString("update %1 id %2").arg(upd.message.text).arg(upd.message.message_id);
            if(upd.update_id >= m_lastUpdateId) {
                m_lastUpdateId = upd.update_id + 1;
            }
            if(upd.message.message_id != -1) {
                cmd_t c = CommandGenerator::generate(0x33, addr_own, parseCommand(upd.message));
                if(CMD_NONE != c.cmd) {
                    m_tempStorage.insert(c.uid, upd.message);
                    emit s_received_cmd(c);
                }
            }
        }
    }

    waitUpdate();
}

// ----------------------------------------------------------------------------
void TgGate::waitUpdate() {

    if(m_reqTime.isValid() && m_reqTime.elapsed() < 300) {
        if(!m_updating) {
            m_updating = true;
            QTimer::singleShot(500, this, [=](){ getUpdates(m_lastUpdateId, 100, 60); });
        }
    }
    else {
        getUpdates(m_lastUpdateId, 100, 60);
    }
}

// ----------------------------------------------------------------------------
template <>
void TgGate::handleObject<TgGate::Message>(const TgGate::Message &, bool) {

}

// ----------------------------------------------------------------------------
void TgGate::slotError(QNetworkReply::NetworkError) {

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());
    Q_ASSERT(reply);
    qCDebug(tgGate) << QString("error: %1").arg(reply->errorString());
}

// ----------------------------------------------------------------------------
void TgGate::slotSslErrors(const QList<QSslError> &lst) {

    QString str;
    for(QSslError error : lst) {
        str.append(QString("%1").arg(error.errorString()));
    }
    qCWarning(tgGate) << QString("ssl errors: %1").arg(str);
}

// ----------------------------------------------------------------------------
QString TgGate::generateWwwFormUrlencoded(const std::vector<Arg>& args) {
    QString result;

    bool firstRun = true;
    for (const Arg& item : args) {
        if (firstRun) {
            firstRun = false;
        } else {
            result += '&';
        }
        result += QUrl::toPercentEncoding(item.name);
        result += '=';
        result += QUrl::toPercentEncoding(item.value);
    }
    return result;
}

// ----------------------------------------------------------------------------
QString TgGate::generateRandomString(size_t length) {
    static const QString chars("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890-=[]\\;',./!@#$%^&*()_+{}|:\"<>?`~");
    static const size_t charsLen = chars.length();
    QString result;
    for (size_t i = 0; i < length; ++i) {
        result += chars[rand() % charsLen];
    }
    return result;
}

// ----------------------------------------------------------------------------
TYPE_CMD TgGate::parseCommand(const Message &mes) {

    QString str = mes.text.trimmed();
    QStringList lst = str.split(" ", QString::SkipEmptyParts);
    if(!lst.isEmpty()) {
        QString cmd = lst.at(0);
        if(!cmd.isEmpty() && (cmd.at(0) == '/')) {
            cmd = cmd.right(cmd.length()-1);
            if("start" == cmd) {
                sendMessage(mes.chat.id, QString::fromUtf8("Hello!"), 0);
                return CMD_NONE;
            }
            // TODO help
            return StringCmdConv::decode(cmd);
        }
    }

    return CMD_NONE;
}
