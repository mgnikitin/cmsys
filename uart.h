/* 
 * File:   uart.h
 * Author: user
 *
 * Created on 1 Февраль 2015 г., 16:13
 */

#ifndef UART_H
#define	UART_H

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <QString>
#include <QSocketNotifier>
#include <QObject>

#include "trace.h"

/** \brief Скорость обмена последовательного порта, бод
 */
enum TTY_BAUDRATE
{
    BD4800 = B4800,         ///< \brief 4800
    BD9600 = B9600,         ///< \brief 9600
    BD19200 = B19200,       ///< \brief 19200
    BD38400 = B38400,       ///< \brief 38400
    BD57600 = B57600,       ///< \brief 57600
    BD115200 = B115200      ///< \brief 115200
};

/** \brief Управление потоком последовательного порта
  */
enum TTY_FLOW
{
    FLOW_NONE = 0,                      ///< \brief нет управления
    FLOW_HARD = CRTSCTS,                ///< \brief аппаратное управление
    FLOW_SOFT = IXON | IXOFF | IXANY    ///< \brief программное управление
};

/** \brief Бит четности последовательного порта
  */
enum TTY_PARITY
{
    NO_PAR = 0,                 ///< \brief нет бита четности
    EVEN = PARENB,              ///< \brief even
    ODD = PARENB | PARODD,      ///< \brief odd
    SPACE = 0                   ///< \brief space
};

/** \brief Количество бит данных
 */
enum TTY_DATA
{
    D5 = CS5,       ///< \brief 5 бит
    D6 = CS6,       ///< \brief 6 бит
    D7 = CS7,       ///< \brief 7 бит
    D8 = CS8        ///< \brief 8 бит
};

/** \brief Класс работы с последовательным портом.
  * Перед началом обмена необходимо проинициализировать порт,
  * вызвав функцию \ref Open().
  */
class CUart : public QObject
{
    Q_OBJECT
public:

    /** \brief Параметры последовательного порта.
      * Не все сочетания параметров совместимы между собой.
      */
    struct t_uart_prm
    {
        QString dev;
        TTY_BAUDRATE baudrate;      ///< \brief скорость обмена
        TTY_DATA data;              ///< \brief количество бит данных
        TTY_PARITY parity;          ///< \brief бит четности
        TTY_FLOW flow;              ///< \brief управление потоком
    };

    /** \brief Конструктор
      */
    CUart();

    /** \brief Деструктор
      */
    virtual ~CUart();

    /** \brief Открытие последовательного порта (инициализация устройства)
      * \param dev - путь к устройству
      * \param prm - параметры устройства
      * \return true - последовательный порт упешно открыть, false - ошибка при открытии порта.
      */
    bool Open(QString dev, const t_uart_prm &prm);

    /** \brief Закрытие последовательного порта
     */
    void Close();

    /** \brief Проверка открытия последовательного порта (инициализации устройства)
      * \return true - порт открыт, false - порт не открыт
      */
    bool isOpen() const {
        return (fd != -1);
    }
    
    int GetDescriptor() const {
        return fd;
    }

    /** \brief Наличие поступления новых данных. Флаг сбрасывается при первом чтении
      * данных с последовательного устройства, при этом в буфере устройства могут
      * находится несчитанные данные
      */
    bool isReadyReadData() const {
        return updated_read;
    }

    /** \brief Запись данных в последовательный порт
      * \param p_buf - указатель на массив данных для пердачи в порт
      * \param size - количество передаваемых байт
      * \return количество переданных байт. В случае возникновения ошибки
      * возвращает -1. Код ошибки содержится в errno.
      * Если устройство не проинициализировано успешно - возвращает 0.
      */
    ssize_t write(const char *p_buf, size_t count);

    /** \brief Запись данных в последовательный порт,
      * аналогично \ref write(const char *p_buf, size_t count)
      */
    ssize_t write(const QByteArray &ba);

    /** \brief Чтение данных с последовательного порта
      * \param p_buf - указатель на массив, заполняемый данными
      * \param size - доступный размер массива
      * \return количество считанных байт. В случае возникновения ошибки
      * возвращает -1. Код ошибки содержится в errno. EAGAIN не является
      * ошибкой и означает отсутсвие данных в последовательном устройстве.
      * Если устройство не проинициализировано успешно - возвращает 0.
      * Отслеживание поступления данных на последовательный порт можно,
      * подключив сигнал \ref s_ready_read().
      */
    ssize_t readAll(char *p_buf, size_t size);

private:

    /// \brief Файловый дескриптор последовательного порта
    int fd;

    /// \brief Предыдущие параметры последовательного порта для POSIX-интерфейса
    termios old_tio;

    /// \brief Новые параметры последовательного порта для POSIX-интерфейса
    termios new_tio;

    /// \brief Параметры последовательного порта
    t_uart_prm setting;

    /// \brief Нотифер поступления данных на чтения из файла
    QSocketNotifier *pfd_notif;
    QSocketNotifier *pfd_except_notif;

    /** \brief Флаг поступления данных для чтения. Флаг сбрасывается при первом чтении
      * данных с последовательного устройства, при этом в буфере устройства могут
      * находится несчитанные данные.
      */
    bool updated_read;

    bool create_notifier(QSocketNotifier **psn, QSocketNotifier::Type type);

    void delete_notifier(QSocketNotifier **psn);

    /** \brief Открыть файл, представляющий собой последовательный порт в операционной системе.
      * \param device_name - путь к файлу
      * \return true - файл успшно открыт, false - ошибка при открытии файла,
      * errno содержит код ошибки.
      */
    bool open_fd(const QString &device_name);

    /** \brief Закрыть файл
      * \return true - файл закрыт успешно, false - ошибка при закрытии файла,
      * errno содержит код ошибки
      */
    bool close_fd();

    /** \brief Установить параметры последовательного порта
      * (использует POSIX-функцию)
      */
    bool set_setting();

    /** \brief Восстановить предыдущие параметры последовательного порта
      * (использует POSIX-функцию)
      */
    bool restore_setting() const;

private slots:

    /** \brief Обработчик сигнала QSocketNotifier
      */
    void slot_notifier_activated(int);

    void slot_except_notifier_activated(int);

signals:

    /** \brief Сигнал поступления данных на последовательный порт
      */
    void s_ready_read();

    void s_exception();
};

#endif	/* UART_H */

