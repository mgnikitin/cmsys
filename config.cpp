/* 
 * File:   config.cpp
 * Author: user
 * 
 * Created on 28 Январь 2015 г., 22:42
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "trace.h"

// -----------------------------------------------------------------------------
char* ltrim(char* s)
{
    char* newstart = s;
    
    if (!s)
        return s;

    while (isspace( *newstart)) {
        ++newstart;
    }

    // newstart points to first non-whitespace char (which might be '\0')
    memmove( s, newstart, strlen( newstart) + 1); // don't forget to move the '\0' terminator

    return s;
}

// -----------------------------------------------------------------------------
t_config Config::defaultConfig() {

    t_config out;
    out.uart_dev = std::string("/dev/ttyS0");
    out.uart_bd = 9600;
    out.mk_addr = 0x02;
    out.addr_own = 0x10;
    out.db_name = std::string("cmd");
    out.db_host = std::string("localhost");
    out.db_user = std::string("user");
    out.db_password = std::string("user");
    out.cam_user = std::string("user");
    out.cam_password = std::string("user");

    return out;
}

// -----------------------------------------------------------------------------
bool Config::readConfig(const char *filepath, t_config *cfg) {
    
    FILE *pfile;
    char str[255];
    char *str_trim;
    char prm[255];
    char val[255];
    
    trace(L_TRACE, "ReadConfig(%s)", filepath);
    
    // open file
    pfile = fopen(filepath, "r");
    if(!pfile)
    {
        trace(L_ERROR, "Couldn't open config file '%s': %s (%d)", 
                filepath, strerror(errno), errno);
        return false;
    }
    
    while(true)
    {
        if(!fgets(str, 255, pfile))
        {
            if(ferror(pfile) != 0)
                trace(L_ERROR, "Couldn't read from config file '%s'", filepath);
            break;
        }
        str_trim = ltrim(&str[0]);
        //trace(L_TRACE, "str_trim: %s", str_trim);
        if(strlen(str_trim) == 0)
            continue;
        if('#' == str_trim[0])
            continue;
        
        if(sscanf(str_trim, "%s %s", prm, val) != 2)
            trace(L_ERROR, "Couldn't parse string '%s'", str_trim);
        else
        {
            if(strcmp(prm, "uart_dev") == 0)
                cfg->uart_dev = std::string(val);
            else if(strcmp(prm, "uart_bd") == 0) {
                int i;
                if(sscanf(val, "%d", &i) == 1)
                    cfg->uart_bd = i;
                else {
                    trace(L_ERROR, "Couldn't parse uart_bd '%s'", val);
                }
            }
            else if(strcmp(prm, "mk_addr") == 0) {
                unsigned char uc;
                if(sscanf(val, "%hhu", &uc) == 1)
                    cfg->mk_addr = uc;
                else {
                    trace(L_ERROR, "Couldn't parse mk_addr '%s'", val);
                }
            }
            else if(strcmp(prm, "own_addr") == 0) {
                unsigned char uc;
                if(sscanf(val, "%hhu", &uc) == 1)
                    cfg->addr_own = uc;
                else {
                    trace(L_ERROR, "Couldn't parse own_addr '%s'", val);
                }
            }
            else if(strcmp(prm, "db_host") == 0) {
                cfg->db_host = std::string(val);
            }
            else if(strcmp(prm, "db_name") == 0) {
                cfg->db_name = std::string(val);
            }
            else if(strcmp(prm, "db_user") == 0) {
                cfg->db_user = std::string(val);
            }
            else if(strcmp(prm, "db_password") == 0) {
                cfg->db_password = std::string(val);
            }
            else if(strcmp(prm, "tg_token") == 0) {
                cfg->tg_token = std::string(val);
            }
            else if(strcmp(prm, "cam_host") == 0) {
                cfg->cam_host = std::string(val);
            }
            else if(strcmp(prm, "cam_user") == 0) {
                cfg->cam_user = std::string(val);
            }
            else if(strcmp(prm, "cam_password") == 0) {
                cfg->cam_password = std::string(val);
            }
        }
    }
    
    // close file
    if(EOF == fclose(pfile))
    {
        trace(L_ERROR, "Couldn't close config file '%s': %s (%d)", 
                filepath, strerror(errno), errno);
    }    
    
    return true;
}


